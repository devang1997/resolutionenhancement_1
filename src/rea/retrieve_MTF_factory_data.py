"""
This is a set of functions to retrieve MTF & other RATP data from the factory database.
This also has a functions that can plot the MTF data.

TODO: It would be better to break out the MTF-specific stuff from the generic factory
      database retrieval stuff.

How to find S/N's for a particular product type:
    Use this form to generate a report or CSV of the receptors of interest:
    http://slfpweb2.vms.ad.varian.com:8093/Receptor_Test_Reports/

How to find factory test data for an Imager S/N 146S07-1803:
    If you don't find the data on:
    http://slfpweb2.vms.ad.varian.com:8093/Imagers/1---/14--/146S07-1803/

    Then it was probably moved to the archive on:
    http://slfpfs/fptestdata-archive/Imagers/1---/14--/146S07-1803/

    Once you find the base directory, MTF test data is often at:
        /final/pass_1/Receptor%20Test%20Data/MTF/   *.mtf, *.txt, *.viv
    or sometimes at:
        /final/pass_1/Receptor_Test_Data/MTF/
    or RMAs can be at:
        /rma/60001361/final/pass_1/Receptor%20Test%20Data/MTF/

"""
# built in to python
try:
    from urllib.parse import urljoin # python 3
except ImportError:
    from urlparse import urljoin # python 2
import re
import collections
import json

# 3rd party (part of Anaconda unless explicitly noted)
import requests # HTTP library
from bs4 import BeautifulSoup # Beautiful Soup HTML parser
import matplotlib.pyplot as plt
try:
    import seaborn as _sns
    _sns.set_context('talk') # {paper, notebook, talk, poster} scaling
    _sns.set_style('whitegrid') # {darkgrid, whitegrid, dark, white, ticks}
except ImportError:
    pass


factory_imager_locs = collections.OrderedDict((
    ('inprogress', 'http://slfpweb2.vms.ad.varian.com:8093/Imagers/'),
    ('archived', 'http://slfpfs/fptestdata-archive/Imagers/'), # a.k.a. http://slfpfs.vms.ad.varian.com/fptestdata-archive/Imagers/
))
TIMEOUT = 5.0 # default timeout in seconds
DEBUG = False


def persist_to_file(file_name):
    # Caching decorator that persists to a json file:
    #   http://stackoverflow.com/questions/16463582/memoize-to-disk-python-persistent-memoization
    # Use by decorating the function you want to cache like so:
    #   @persist_to_file('cached_func.cache')
    def decorator(original_func):
        try:
            cache = json.load(open(file_name, 'r'))
        except (IOError, ValueError):
            cache = {}
        def new_func(param):
            if param not in cache:
                cache[param] = original_func(param)
                json.dump(cache, open(file_name, 'w'))
            return cache[param]
        return new_func
    return decorator




def plot_factory_mtfs(serial_nums, title=''):
    '''Plots list of panel serial_nums as MTF curves, extracted from factory test archives.

    Example:
    >> plot_factory_mtfs(['239S07-2004','247S06-0203'])
    '''
    if isinstance(serial_nums, str):
        serial_nums = [serial_nums] # allow passing of single string instead of list

    # plotting code based on query_MTF_NPS.plot_MTFs()
    plt.close()
    fig_scale = 1.1
    fig_title = 'MTF Comparison%s' % (' - '+title if title else '')
    fig = plt.figure(fig_title, figsize=(16 * fig_scale, 9 * fig_scale))
    sns.set_palette('husl', max(len(serial_nums), 5)) # {deep, muted, bright, pastel, dark, colorblind}, {hls, husl}, or any matplotlib.org/users/colormaps.html
    n_valid_curves = 0
    n_valid_serials = 0
    for serial in serial_nums:
        fac_mtfs = get_factory_mtfs(serial)
        if fac_mtfs:
            n_valid_serials += 1
        for fac_mtf in fac_mtfs:
            if fac_mtf:
                name = fac_mtf['name']
                freq = fac_mtf['freq']
                MTF = fac_mtf['mtf']
                pixel_size_mm = fac_mtf['pixel_size_mm']
                is_rma = fac_mtf['is_rma']
                n_valid_curves += 1
            else:
                continue
            label_text = '%s_%s' % (name, serial, )
            if pixel_size_mm>0.0:
                label_text += ' [%smm]' % (pixel_size_mm, )
            if is_rma:
                label_text += ' [RMA]'
            #plt.plot(freq, MTF, '.-', label=label_text)
            plt.plot(freq, MTF, '-H', label=label_text, linewidth=1.0, alpha=0.6, markersize=5.0)
            #sns.tsplot(MTF, time=freq)
    print('Found FATP and/or RMA final test data for %d of %d (%.1f%%) provided serial numbers' % (n_valid_serials, len(serial_nums), n_valid_serials*100.0/len(serial_nums)))
    sns.despine()
    plt.ylabel('MTF')
    plt.xlabel('Frequency [cycles/mm]')
    plt.legend(loc='upper right')
    if title:
        plt.title(title)
    # plt.grid()
    if n_valid_curves>0:
        plt.show()

@persist_to_file('get_factory_mtfs.cache')
def get_factory_mtfs(serial_num):
    url = find_url_for_receptor(serial_num)
    if not url:
        return []

    links = extract_link_trees_mtf(url)
    mtflinks = [x for x in links if x.endswith('.txt')]
    if not mtflinks:
        print('no factory MTF test results found')
        return []
    #if DEBUG: print('MTF txt files:\n%s' % mtflinks)

    fac_mtfs = list()
    for mtflink in mtflinks:
        name = mtflink.split('/')[-1].rstrip('.txt')
        is_rma = mtflink.lower().count('/rma') > 0
        req = requests.get(mtflink, timeout=TIMEOUT)
        lines = req.text.split('\r\n')
        # this text is often in this type of format (tab-delimited, \r\n line endings, header)
            # MTF info file:
            # File name: MTF_sum150.viv
            # ROI specified: (782, 675) to (1400, 1138)

            # Number of points used in calculation was 8192
            # Number of points used in calculation was 8192
            # No windowing was applied
            # Gradient of edge/slit was 1 pixel in 30
            # Pixel size used in calculation was   0.127mm
            # Frequency increment was   0.029mm^-1 (=30/(8192*  0.127))

            # cycles/mm   MTF
            #      0.000       1.000
            #      0.029       0.965
            #      0.058       0.938
            #      0.087       0.928
            #      ...
        in_data = False
        pixel_size_mm = 0.0
        freq = []
        MTF = []
        for (n, line) in enumerate(lines):
            # if DEBUG: print('processing line %d "%s"' % (n, line))
            if in_data:
                cols = line.split('\t')
                if len(cols) < 2: continue
                freq += [float(cols[0])]
                MTF += [float(cols[1])]
            else:
                line = line.lower()
                if line.startswith('pixel size'):
                    pix_sz_str = line.split(' ')[-1]
                    pixel_size_mm = float(pix_sz_str.replace('mm',''))
                if line.startswith('cycles'):
                    in_data = True
        fac_mtfs.append({'Panel_type': name, 'Freq_cycles_per_mm': freq,
                         'MTF': MTF, 'Pixel_size_mm': pixel_size_mm,
                         'is_rma': is_rma})
    return fac_mtfs

def canonicalize_serial(serial_num_in):
    '''Tries to convert a serial number into one of 2 typical factory-standard forms:
    Form 1: dddSdd-dddd such as '146S07-1803', typ length 11
    Form 2: (s|d)ddd-dd[c] such as 'D969-04A' or 'C777-02' or '9704-02B', typ length 7-8
    '''
    serial_num = serial_num_in.upper()
    serial_num = serial_num.split(' ')[0] # discard all inputs after 1st space
    serial_num = '-'.join(serial_num.split('-')[-2:]) # only keep last 2 dash-divided segments
    if len(serial_num) > 10: # ignore leading strings, bulding from back
        # Form 1
        if serial_num.count('-')<1 or len(serial_num)>11:
            serial_num = serial_num.replace('-','')
            serial_num = serial_num[-10:-4]+'-'+serial_num[-4:]
    else:
        # Form 2
        if serial_num.count('-')<1 or len(serial_num)>8:
            serial_num = serial_num.replace('-','')
            if serial_num[-1].isalpha():
                serial_num = serial_num[-7:-3]+'-'+serial_num[-3:]
            else:
                serial_num = serial_num[-6:-2]+'-'+serial_num[-2:]
    return serial_num

def find_url_for_receptor(serial_num_in, verbose=False):
    # '146S07-1803' becomes '1---/14--/146S07-1803/'
    serial_num = canonicalize_serial(serial_num_in)
    if verbose: print('%s: ' % serial_num, end='')
    prefix = '%s---/%s%s--/' % (serial_num[0], serial_num[0], serial_num[1])
    for (loc, base) in factory_imager_locs.items():
        url = urljoin(base, prefix)
        url = urljoin(url, serial_num+'/')
        if DEBUG: print('\nurl candidate %s:\n\t%s' % (loc, url))
        try:
            req = requests.get(url, timeout=TIMEOUT)
            if req.status_code == requests.codes.ok:
                if verbose: print(url)
                return url
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            if DEBUG: print('\nconnnection error to %s' % url)
    if verbose: print('not found')
    return ''

def extract_links(url, match=''):
    if DEBUG: print('extracting links that match "%s" from %s' % (match, url))
    try:
        req = requests.get(url, timeout=TIMEOUT)
        if req.status_code != requests.codes.ok:
            return []
        soup = BeautifulSoup(req.text)
        links = []
        for link in soup.find_all('a'):
            if link.has_attr('href'):
                h = link['href']
                if re.match(match, h):
                    if DEBUG: print('link extracted "%s"' % h)
                    links += [urljoin(url, h)]
        return links
    except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
        return []

def extract_link_tree(url, regex_tree):
    '''Finds links in a tree of HTML documents linked to from URL that match with
    the paths specified by tuple REGEX_TREE.

    Examples:
        import retrieve_MTF_factory_data as ret

        links1 = ret.extract_link_tree('http://www.varian.com/', '.*products.*detector.*')

        url = ret.find_url_for_receptor('402S01-0204')
        reg = ('^final.*','^pass.*',r'^Receptor(%20|_)Test(%20|_)Data.*','^Dark.*','.*\.viv$')
        links2 = ret.extract_link_tree(url, reg)
    '''
    if isinstance(regex_tree, str): # allow calling with a single regex
        regex_tree = [regex_tree]
    links = [url]
    for regex in regex_tree:
        links = extract_links(links[0], match=regex)
        if len(links)==0:
            if DEBUG: print('quitting early -- could not find content under "%s%s*/"' % (url, regex))
            break
    return links

def extract_link_trees(url, regex_trees=[('',)]):
    '''Finds links matching a list of tuple REGEX_TREES.

    Examples:
        import retrieve_MTF_factory_data as ret
        links1 = ret.extract_link_trees('https://www.varian.com/', ('.*products.*detector.*', '.*.jp(e?)g$'))
    '''
    if isinstance(regex_trees[0], str): # allow calling with a single regex tree
        regex_trees = [regex_trees]
    links = []
    for tree in regex_trees:
        links += extract_link_tree(url, regex_tree=tree)
    return links

MTF_REGEX_1 = ('^final.*','^pass.*',r'^Receptor(%20|_)Test(%20|_)Data.*','^MTF.*','.*\.(mtf|viv|txt)$')
MTF_REGEX_TREES = [MTF_REGEX_1, ('^(rma|RMA).*','^\d{8}.*')+MTF_REGEX_1]
def extract_link_trees_mtf(url, regex_trees=MTF_REGEX_TREES):
    '''Defaults that find MTF test text results in factory data'''
    return extract_link_trees(url, regex_trees=regex_trees)

def download_links(links, chunk_size=5000, out_path='.', force_redownload=False):
    import os, sys
    n_downloaded = 0
    if links:
        for url in links:
            filename = url.split('/')[-1]
            if not os.path.exists(out_path):
                os.makedirs(out_path)
            full_file_path = os.path.join(out_path, filename)
            if not os.path.exists(full_file_path) or force_redownload:
                print('Downloading file "%s" ... ' % filename, end='')
                sys.stdout.flush() # make this show up
                r = requests.get(url, stream=True)
                with open(full_file_path, 'wb') as fd:
                    for chunk in r.iter_content(chunk_size):
                        fd.write(chunk)
                print('done.')
                n_downloaded += 1
    return n_downloaded




