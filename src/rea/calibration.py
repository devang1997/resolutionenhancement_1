import sqlite3 as _sqlite3
import matplotlib.pyplot as _plt
import numpy as _np
import os as _os
from .encrypt_calibration_data \
    import generate_encrypted_base64_data, recover_data
import datetime as _datetime
import collections as _collections
import re as _re
import logging as _logging
# Alan's retrieval functions
from .retrieve_MTF_factory_data \
    import find_url_for_receptor, extract_link_tree, get_factory_mtfs

try:
    import seaborn as _sbn
    _sbn.set_style('darkgrid')
    _sbn.set_context('talk')
except ImportError:
    pass

_k_reacalib_file_path = _os.path.dirname(_os.path.abspath(__file__))
_k_encryption_scheme = 'xor_base64_salt'
_k_exclude_table = 'sqlite_sequence'

Sqlite3Format = _collections.namedtuple("Sqlite3Format",
                                        ["filename", "mtf_nps", "customers"])
# =============================================================================


class ResolEnhAlgCalib:
    """
    REA Calibration
    ~~~~~~~~~~~~~~~~

    Resolution Enhancement Algorithm calibration package,
     which has a set of functionalities to
    1) access the MTF / NPS sqlite file
    2) insert new MTF / NPS from SLC panel test url or from user input
    2) generate encrypted REA xml (calibration) files for CST

    :Note:
    For the SLC panel test data access method, the MTF retrieval is not
     fully automated, in the sense that a serial number needs to be
     manually retrieved from
     http://slfpweb2.vms.ad.varian.com:8093/Receptor_Test_Reports
     (using a web-based tool), and the MTFs can be retrieved based on the
     serial number information.

    Accessing SLC test data via url is done using Alan Brook's code:
     retrieve_MTF_factory_data.py

    The decision to keep only the most recent query of the sqlite DB is
    intentional; there is no need to keep the whole table as the table
    grows in the future.

    :copyright: 2015-,  Sungwon Yoon, see AUTHORS for more details
    :license: license_name, see LICENSE for more details

    """

    def __init__(self, sqlite3_file=None):

        self._db_connect = None
        self._cursor = None
        # Table information, such as structures
        self._table_info = dict()
        # MTFs recently pulled from the factory data
        self._mtfs_pulled_from_online = None

        if not sqlite3_file:
            sqlite3_file = "{}/../../data/detector_panel_MTF_NPS.sqlite".\
                format(_k_reacalib_file_path)
        self._sqlite3_file = Sqlite3Format(_os.path.abspath(sqlite3_file),
                                           'PANEL_MTF_NPS', 'CUSTOMERS')

        # Change the working path to where the DB is located
        path_file = _os.path.split(self._sqlite3_file.filename)
        if path_file[0]:
            _os.chdir(path_file[0])

        try:
            # In order to make any operation with the database we need to get a cursor
            # object and pass the SQL statements to the cursor object to execute them
            self._db_connect = _sqlite3.connect(self._sqlite3_file.filename)

            # From https://docs.python.org/3/library/sqlite3.html#sqlite3.Row
            # A Row instance serves as a highly optimized row_factory for Connection objects. It tries to mimic a tuple
            #  in most of its features.
            # It supports mapping access by column name and index, iteration, representation, equality testing and len().
            # If two Row objects have exactly the same columns and their members are equal, they compare equal.
            # keys()
            # This method returns a list of column names. Immediately after a query, it is the first member of each
            #  tuple in Cursor.description.
            self._db_connect.row_factory = _sqlite3.Row

            self._cursor = self._db_connect.cursor()

            # Query sqlite version
            self._cursor.execute('SELECT SQLITE_VERSION()')
            _logging.info("Connection to sqlite3 DB ({}) accomplished. "
                          "Using sqlite3 version {}".format(
                self._sqlite3_file.filename, self._cursor.fetchone()[0]))

            ## DEBUG: show everything
            #cursor.execute("SELECT * FROM sqlite_master")
            #print(cursor.fetchall())

            # Get tables in the database
            self._get_tables_and_properties()

        except:
            raise IOError("Cannot open sqlite3 DB file {}".format(
                self._sqlite3_file.filename
            ))
            self._db_connect.close()

    def __del__(self):
        self._db_connect.close()

    # ---------------------------
    # Property
    # ---------------------------
    @property
    def mtfs_pulled_from_online(self):
        return self._mtfs_pulled_from_online

    #@mtfs_pulled_from_online.setter
    #def mtfs_pulled_from_online(self, value):
    #    pass

    # ------------------------
    #  Private functions
    # ------------------------
    def _get_tables_and_properties(self, remove_sqlite_seq=True):

        sql_query_str = "SELECT name FROM sqlite_master WHERE type='table'"
        list_tables = self.query(sql_query_str)

        for cur_table in list_tables:
            if cur_table[0] in _k_exclude_table:
                continue
            sql_query_str = "PRAGMA table_info({})".format(cur_table[0])
            data = self.query(sql_query_str)

            table_name = cur_table[0]  #tuple(cur_table)[0]
            self._table_info[table_name] = \
                ResolEnhAlgCalib._convert_sqlite3_rows_to_list(data)

    def _get_columns_of_table(self, table_name):
        if self._table_info:
            self._get_tables_and_properties()

        assert(table_name in self.get_list_tables())

        list_of_columns = None
        for key, val in self._table_info.items():
            if key.lower() == table_name.lower():
                list_of_columns = [elem['name'] for elem in val]
                break

        return list_of_columns

    def _num_rows(self, table_name):
        """
        Return the current number of rows in the user specified table.

        Args:
            table_name:

        Returns:

        """
        sql_query_str = "SELECT * FROM {}".format(table_name)
        rows = self.query(sql_query_str)
        return len(rows)

    def _fetch_from_online_and_append_into_db(self, serial_num, panel_type,
                                              scintillator, **kwargs):
        """
        Append to the the sqlite DB information with

        Args:
            serial_num: one single serial number
            serial_num: str
            panel_type: e.g. "3030D"
            scintillator: e.g. "CsI (600um)"
            notes : string to add to the notes column
                    it is recommended to provide the receptor model & rev
                    e.g. notes="3030D CBM Rev AD 33832"

        Returns:

        """
        selected_panel_info = self._mtfs_pulled_from_online[serial_num]

        self._append_mtf_nps_into_db(serial_num, panel_type, scintillator,
                                     selected_panel_info['Pixel_size_mm'],
                                     selected_panel_info['Freq_cycles_per_mm'],
                                     selected_panel_info['MTF'],
                                     **kwargs)

    def _get_max_unique_id(self, table_name):
        # Using ROWID as surrogate for xml_id in CUSTOMERS table and
        #  Panel_id in PANEL_MTF_NPS table. Both columns are unique INTEGERS!
        sql_query_str = "SELECT ROWID from {}".format(table_name)
        result = self.query(sql_query_str)
        if result:
            result = [elem[0] for elem in result]
            result = max(result)
        else:
            result = 0
        return result

    def _append_mtf_nps_into_db(self, serial_num, panel_type, scintillator,
                                pixel_size, freq, mtf, **kwargs):
        """
        Append to the the sqlite DB information with

        Args:
            serial_num: one single serial number
            serial_num: str
            panel_type: e.g. "3030D"
            scintillator: e.g. "CsI (600um)"
            pixel_size: (float) pixel size in mm (assume isotropic pixel size)
            freq: (list or numpy array) sampled freq points for the MTF (and NPS)
            mtf: (list or numpy array) MTF values at the sampled freq points

            freq_nps: (optional)
                      sampled freq points for NPS values, which are different
                      from the sampled freq points for MTF values
            nps: (optional)
                 (list or numpy array) NPS values at the sampled freq points
            notes: (optional)
                    string to add to the notes column
                    it is recommended to provide the receptor model & rev
                    e.g. notes="3030D CBM Rev AD 33832"

        Returns:

        """
        assert(len(freq) == len(mtf))
        nps = None
        freq_nps = None
        if 'nps' in kwargs:
            nps = kwargs['nps']
        if 'freq_nps' in kwargs:
            freq_nps = kwargs['freq_nps']
        if 'nps_freq' in kwargs:
            freq_nps = kwargs['nps_freq']

        if nps is not None:
            if freq_nps is not None:
                assert(len(freq_nps) == len(nps))
            else:
                assert(len(freq) == len(nps))


        # New item id number
        unique_id = self._get_max_unique_id(self._sqlite3_file.mtf_nps) + 1

        panel_info = self.get_panel_with_serial_num(serial_num)
        # Don't want 2 items with the same serial number
        if panel_info is None:

            # B) Tries to insert an ID (if it does not exist yet)
            # with a specific value in a second column
            freq_str = ', '.join([str(elem) for elem in freq])
            mtf_str = ', '.join([str(elem) for elem in mtf])

            # The following columns cannot be NULL
            #  Panel_id, Panel_type, Scintillator, Freq_cycles_per_mm, MTF
            cmd_str = "INSERT INTO {} (Panel_id, Panel_type, Serial_num,\
            Scintillator, Pixel_size_mm, Freq_cycles_per_mm, MTF) VALUES \
            ('{}', '{}', '{}', '{}', '{}', '{}', '{}')". \
                format(self._sqlite3_file.mtf_nps, unique_id, panel_type,
                       serial_num, scintillator, pixel_size,
                       freq_str, mtf_str)
            self._cursor.execute(cmd_str)

            if nps is not None:
                nps_str = ', '.join([str(elem) for elem in nps])
                cmd_str = "UPDATE {} SET NPS ='{}' WHERE Panel_id={}". \
                    format(self._sqlite3_file.mtf_nps, nps_str, unique_id)
                self._cursor.execute(cmd_str)
            if freq_nps is not None:
                nps_freq_str = ', '.join([str(elem) for elem in freq_nps])
                cmd_str = "UPDATE {} SET Freq_cycles_per_mm_NPS ='{}' WHERE Panel_id={}". \
                    format(self._sqlite3_file.mtf_nps, nps_freq_str, unique_id)
                self._cursor.execute(cmd_str)

            # Add the information in the Notes column
            if 'notes' in kwargs:
                notes = kwargs['notes']
                cmd_str = "UPDATE {} SET Notes='{}' WHERE Panel_id={}". \
                    format(self._sqlite3_file.mtf_nps, notes, unique_id)
                self._cursor.execute(cmd_str)

            self._db_connect.commit()

        else:
            print("Panel with S/N {} already exists in the DB {} with Panel "
                  "ID {}!".format(serial_num, self._sqlite3_file.filename,
                                  panel_info["Panel_id"]))


    def _delete_from_table(self, table_name, column_name, value):
        """
        Delete a "row" from the sqlite3 DB with specified column name and value.

        Args:
            table_name:
            column_name:
            value:

        Returns:

        """
        cmd_str = "DELETE FROM {} WHERE {} = '{}'".format(
            table_name, column_name, value
        )
        self._cursor.execute(cmd_str)
        self._db_connect.commit()


    def _get_rows_from_table_that_satisfy(self, table_name, **kwargs):
        """

        Args:
            table_name:
            **kwargs:
                'Panel_id', ... for PANEL_MTF_NPS table
                'xml_id', ... for CUSTOMERS table

        Returns:
            panel_info: (list)

        """
        if table_name.upper() == self._sqlite3_file.mtf_nps:
            fixed_keys = self._get_columns_of_table(self._sqlite3_file.mtf_nps)
            table_name = self._sqlite3_file.mtf_nps
        elif table_name.upper() == self._sqlite3_file.customers:
            fixed_keys = self._get_columns_of_table(self._sqlite3_file.customers)
            table_name = self._sqlite3_file.customers
        else:
            raise RuntimeError("Specified table {} unknown.".format(table_name))

        where_clause = ""
        # Loop over user-specified criteria
        for key, val in kwargs.items():
            # Note that user-specified keyword should be close to the fixed keywords
            if key.lower() in [elem.lower() for elem in fixed_keys]:
                # Important to use the LIKE clause to pick up all panels / customers
                #  that contain the user-specified string
                curr_str = "{} LIKE '%{}%'".format(key, val)
                if key.lower() == "panel_id":
                    curr_str = "{} == {}".format(key, val)
                # Build up search clause
                if len(where_clause) > 0:
                    where_clause = "{} AND {}".format(where_clause, curr_str)
                else:
                    where_clause = curr_str

            else:
                print("Specified column field {} cannot be found in table {}.\n"
                      "Available columns are: {}".format(key, table_name,
                                                         fixed_keys))

        list_info = None
        if len(where_clause) > 0:
            sql_query_str = "SELECT * from {} WHERE {}".format(
                table_name, where_clause)
            rows = self.query(sql_query_str)

            # Convert to list of dict for easier access
            list_info = ResolEnhAlgCalib._convert_sqlite3_rows_to_list(rows)

        return list_info

    @staticmethod
    def _convert_sqlite3_rows_to_list(sqlite3_row_obj):
        """

        Args:
            sqlite3_row_obj: (sqlite3.Row object or a list of sqlite3.Row objects)

        Returns:
        """
        if not isinstance(sqlite3_row_obj, list):
            sqlite3_row_obj = [sqlite3_row_obj]
        out_list = list()
        if len(sqlite3_row_obj) > 0:
            for item in sqlite3_row_obj:
                curr_dict = dict()
                for (key, val) in zip(item.keys(), tuple(item)):
                    curr_dict[key] = val
                out_list.append(curr_dict)

        # List of digits as a string converted to list of digits
        match = _re.compile("^[\d\.]+\s*,\s*[\d\.]+\s*,\s*")
        for curr_dict in out_list:
            for key, val in curr_dict.items():
                if isinstance(val, str) and ',' in val:
                    result = match.search(val)
                    if result is not None:
                        converted_list = list()
                        for item in val.split(','):
                            converted_list.append(float(item))
                        curr_dict[key] = converted_list

        return out_list


    @staticmethod
    def _generate_complete_xml_text(mtf_nps_data, enc_str):
        k_calib_section_start_keyword = "<!-- <<<BEGIN>>>  DO NOT EDIT PARAMETERS"
        k_skeleton_file = _os.path.normpath(
            "{}/data/Varex.ResolutionEnhancement.Skeleton.xml".format(
                _k_reacalib_file_path))

        # Generate the calibration section
        xml_file_text = "<ResolutionEnhancementCalibration>\n"
        text = "\t<PanelType>{} (ID: {}, Scintillator: {}, Pixel size: {}mm)</PanelType>\n". \
            format( mtf_nps_data['Panel_type'], mtf_nps_data['Panel_id'],
                    mtf_nps_data['Scintillator'], mtf_nps_data['Pixel_size_mm'])
        xml_file_text = "".join([xml_file_text, text])
        today = _datetime.date.today()
        xml_file_text = "".join([xml_file_text,
                                 "\t<GeneratedDate>{}</GeneratedDate>\n". \
                                format(today.strftime("%Y.%m.%d"))])
        xml_file_text = "".join([xml_file_text,
                                 "\t<CalibrationData>\n{}\t</CalibrationData>\n". \
                                format(enc_str.decode())])
        xml_file_text = "".join([xml_file_text,
                                 "</ResolutionEnhancementCalibration>\n"])

        # Read in the skeleton of the xml file, and find the location to
        # insert the calibration section
        with open(k_skeleton_file, "r") as fid:
            file_contents = fid.readlines()

            fid.seek(0)
            for num, line in enumerate(fid, 1):
                if k_calib_section_start_keyword in line:
                    calib_start_line_no = num
                    break
        # Insert the calibration section
        file_contents.insert(calib_start_line_no, xml_file_text)
        return file_contents


    # =================================
    #   Public functions
    # =================================

    def get_list_tables(self, ):
        list_tables = list()
        for table_name in self._table_info.keys():
            if _k_exclude_table in table_name:
                continue
            list_tables.append(table_name)
        return list_tables

    def query(self, sql_query_str):
        """

        Args:
            sql_query_str: (str)

        Returns:
            sqlite3.Row object or a list of sqlite3.Row objects
        """
        self._cursor.execute(sql_query_str)
        # Return query result
        return self._cursor.fetchall()

    def get_panels_that_have(self, **kwargs):
        """

        Args:
            kwargs:
                'Panel_type'
                'Serial_num'
                'Scintillator'
                'Pixel_size_mm'

        Returns: (list of dict)
            None if no matching items are found
        """
        list_info = self._get_rows_from_table_that_satisfy(
            self._sqlite3_file.mtf_nps, **kwargs)
        return list_info

    def get_panel_with_id(self, panel_id, provide_options=False):
        """
        Retrieves panel info with a unique panel id number.

        Sqlite table PANELS_MTF_NPS is constructed in a way that the Panel_id
         is a Primary key Integer, meaning that "rowid" will be mapped to the
         unique value of Panel_id value.

        Depending on how panel information has been inserted and deleted,
         panel_id number can exceed the number of actual rows in the table.

        Args:
            panel_id: (int)
            provide_options: (bool)

        Returns:
            A single dict with the panel info.
            If no matching panel id is available, a None will be returned,
             and available panel ids in the table will be displayed.

        """
        #panel_info = self._get_panel_with_column("Panel_id", panel_id,
        #                                         provide_options)
        panel_info = self._get_rows_from_table_that_satisfy(
            self._sqlite3_file.mtf_nps, Panel_id=panel_id
        )
        if len(panel_info) == 1:
            panel_info = panel_info[0]
        elif len(panel_info) == 0:
            panel_info = None
        return panel_info

    def get_panel_with_serial_num(self, serial_number, provide_options=False):
        """
        Returns panel information associated with the user provided serial
         number.
        Args:
            serial_number: (str)
            provide_options: (bool)

        Returns:
            panel_info: (dict)
                None if not found
        """
        #panel_info = self._get_panel_with_column("Serial_num", serial_number,
        #                                         provide_options)
        panel_info = self._get_rows_from_table_that_satisfy(
            self._sqlite3_file.mtf_nps, Serial_num=serial_number
        )
        if len(panel_info) == 1:
            panel_info = panel_info[0]
        elif len(panel_info) == 0:
            panel_info = None
        return panel_info

    def get_customer_with_id(self, customer_id):
        customer_info = self._get_rows_from_table_that_satisfy(
            self._sqlite3_file.customers, xml_id=customer_id
        )
        if len(customer_info) == 1:
            customer_info = customer_info[0]
        elif len(customer_info) == 0:
            customer_info = None
        return customer_info

    def get_customers_with_panel_id(self, panel_id):
        customer_info = self._get_rows_from_table_that_satisfy(
            self._sqlite3_file.customers, Panel_id=panel_id
        )
        if len(customer_info) == 1:
            customer_info = customer_info[0]
        elif len(customer_info) == 0:
            customer_info = None
        return customer_info

    def get_customers_with_name(self, customer_name):
        customer_info = self._get_rows_from_table_that_satisfy(
            self._sqlite3_file.customers, Name=customer_name
        )
        if len(customer_info) == 1:
            customer_info = customer_info[0]
        elif len(customer_info) == 0:
            customer_info = None
        return customer_info

    def num_MTFs(self):
        """Return the current number of rows in the MTF DB table.

        """
        return self._num_rows(self._sqlite3_file.mtf_nps)

    def num_customers(self):
        """Return the current number of customers with REA calibration.

        """
        return self._num_rows(self._sqlite3_file.customers)

    def record_calib_generation(self, panel_id, customer_name):
        """
        Record REA calibration generation, identified by the unique panel id,
         in the customers DB.
        Args:
            panel_id: (int)
            customer_name: (str)

        Returns:

        """
        # Get number of items in the customers DB
        unique_id = self._get_max_unique_id(self._sqlite3_file.customers) + 1

        # Add the current mtf_nps_data as being generated for the customer
        fields_txt = ", ".join(self._get_columns_of_table(self._sqlite3_file.customers))
        today = _datetime.date.today()
        date_txt = today.strftime("%Y.%m.%d")
        values_txt = "{}, '{}', {}, '{}'".format(unique_id,
                                                 customer_name,
                                                 panel_id,
                                                 date_txt)
        cmd_str = "INSERT INTO {} ({}) VALUES ({})".format(
            self._sqlite3_file.customers, fields_txt, values_txt)
        self._cursor.execute(cmd_str)
        self._db_connect.commit()

        return self.get_customer_with_id(unique_id)

    def add_new_panel_from_online(self, model_customer_rev_no,
                                  serial_numbers, panel_type, scintillator):
        """Add new mtf information into the sqlite database.

        Args:
            model_customer_rev_no: (str)
                panel model, customer code, and rev number, which can be found
                 on the website at the start of the panel search
                Use the webpage http://slfpweb2.vms.ad.varian.com:8093/Receptor_Test_Reports
                 to find the model_customer_rev_no
            serial_numbers: (list)
                1 or more serial numbers to be inserted into the database
            panel_type: (str)
                e.g., "3030D"
            scintillator: (str)
                e.g., "CsI (600um)"
            notes : (str)
                it is recommended to provide the receptor model & rev
                e.g. notes="3030D CBM Rev AD 33832"

        Returns:
        """
        # Retrieve MTFs
        self.retrieve_factory_mtfs(serial_numbers)

        for curr_sn in serial_numbers:
            self._fetch_from_online_and_append_into_db(
                curr_sn, panel_type, scintillator, notes=model_customer_rev_no)

    def add_new_panel(self, serial_number, panel_type, scintillator,
                      pixel_size, freq, mtf, **kwargs):
        """Add new item into the sqlite database.

            This function assumes that there is no Varex panel physics
            data available. If there exists Varex panel physics data,
            then it may be preferrable to use the function call
            add_new_panel_from_online.

        Args:
            serial_number: (str)
            panel_type: (str)
                        e.g., "0822AP3"
            scintillator: (str)
                        e.g., "CsI (500um)"
            pixel_size: (float) mm unit
                        e.g., 0.200
            freq: (list of floats) cycles / mm unit
            mtf: (list of floats)
                MTF values

            nps: (optional) (list of floats) NPS values
            nps_freq: (optional)
                      (lift of floats) cycles / mm unit
                      Used when a different freq sampling points for NPS measurements
            notes: (optional)
                   string to add to the notes column
                   it is recommended to provide the receptor model & rev
                   e.g. notes="3030D CBM Rev AD 33832"

        Returns:
        """
        self._append_mtf_nps_into_db(serial_number, panel_type, scintillator,
                                     pixel_size, freq, mtf, **kwargs)

    def delete_panel_with_serial_num(self, serial_number):
        """

        Args:
            serial_number: (str)

        Returns:
            panel_info: (dict)
                Deleted panel info as dict, just in case of accidental removal.
                Having the deleted panel info allows an opportunity to put
                 the information back into the DB file.
        """
        panel_info = self.get_panel_with_serial_num(serial_number)
        if panel_info is not None:
            _logging.info("Panel with serial number {} found:".format(
                serial_number))

            for key, val in panel_info.items():
                if not isinstance(val, list) and val:
                    _logging.info("{}: {}".format(key, val))

            self._delete_from_table(self._sqlite3_file.mtf_nps,
                                    "Serial_num", serial_number)
        return panel_info

    def delete_customer_with_id(self, customer_id):
            self._delete_from_table(self._sqlite3_file.customers,
                                    "xml_id", customer_id)

    @staticmethod
    def _plot_mtf_nps(ax, count, panel_info, is_mtf=True, plot_semilogy=False):
        """
        Private function to plot MTF / NPS.
        Args:
            ax:
            count:
            panel_info: (dict)
                        single panel information
            is_mtf: (bool)
            plot_semilogy: (bool)

        Returns:

        """
        k_line_style = '.-'
        if count == 6:
            k_line_style = 'v-'
        k_alpha = 0.7

        # Freq and Spectrum
        freq = panel_info['Freq_cycles_per_mm']
        if not is_mtf and panel_info['Freq_cycles_per_mm_NPS'] is not None:
            freq = panel_info['Freq_cycles_per_mm_NPS']
            assert(len(freq) == len(panel_info['NPS']))
        max_freq = max(freq)
        spectrum = panel_info['MTF' if is_mtf else 'NPS']

        # Label text
        base_label_text ="{} (S/N: {}), {}, {}mm".format(
            panel_info['Panel_type'], panel_info['Serial_num'],
            panel_info['Scintillator'], panel_info['Pixel_size_mm'])
        label_text = "".join([base_label_text,
                              r"     ($f_{{max}}$ = {})".format(
                                  max_freq)])
        if not plot_semilogy:
            ax.plot(freq, spectrum, k_line_style, label=label_text,
                    alpha=k_alpha)
        else:
            ax.semilogy(freq, spectrum, k_line_style, label=label_text,
                    alpha=k_alpha)


    def _plot_mtf_nps_nyq_freq(ax, panel_info, plot_semilogy=False):
        """
        Private function to plot the Nyquist Freq for the set of panels.
        Args:
            panel_info: (list)
            plot_semilogy: (bool)

        Returns:

        """
        k_line_alpha = 0.2
        k_text_alpha = 0.8
        assert(isinstance(panel_info, list))
        # Collect non-repeating Nyquist frequencies based on pixel size
        nyq_freq_list = list()
        for curr_panel in panel_info:
            if curr_panel['Pixel_size_mm'] is not None:
                nyq_freq = 0.5 / _np.float(curr_panel['Pixel_size_mm'])
                if nyq_freq not in nyq_freq_list:
                    nyq_freq_list.append(nyq_freq)
        # Location of the text for Nyquist freq plots
        ymin, ymax = ax.get_ylim()
        mid_vertical = 0.5 * (ymin + ymax)
        if plot_semilogy:
            mid_vertical = 10**(0.5 * (_np.log10(ymin) + _np.log10(ymax)))

        for nyq_freq in nyq_freq_list:
            # Vertical line
            ax.axvline(x=nyq_freq, linestyle='--', color='k',
                       alpha=k_line_alpha)
            # Vertical text
            ax.text(nyq_freq, mid_vertical,
                    "Nyquist Freq = {:.2f}".format(nyq_freq),
                    rotation=90, verticalalignment='center',
                    alpha=k_text_alpha)

    @staticmethod
    def plot_MTF_NPS(panel_info, plot_semilogy=False):
        """
        Plots MTF (and NPS, if it exists) for the panel info provided.
        Args:
            panel_info: (list)
                        list of dict, where each dict holds a single row of
                        information from the sqlite3 DB
            plot_semilogy: (bool)

        Returns:

        """
        k_fig_scale = 1.0
        assert(isinstance(panel_info, list))

        # Determine whether NPS should be plotted
        num_figs = 1
        for curr_panel in panel_info:
            if curr_panel["NPS"] is not None:
                num_figs = 2
                break
        # Prepare subplots
        fig_title = "MTF {}Comparison)".format(
            "" if num_figs == 1 else "/ NPS ")
        #_plt.close(fig_title)
        fig = _plt.figure(fig_title, figsize=(16 * k_fig_scale * num_figs,
                                              9 * k_fig_scale))
        # Set up subplots
        ax1 = _plt.subplot(1, num_figs, 1)
        if num_figs > 1:
            ax2 = _plt.subplot(1, num_figs, 2)

        # Loop over panel information
        for count, curr_panel in enumerate(panel_info):
            # Plot MTF
            ResolEnhAlgCalib._plot_mtf_nps(ax1, count, curr_panel,
                                           True, plot_semilogy)
            # Plot NPS
            if num_figs > 1:
               ResolEnhAlgCalib._plot_mtf_nps(ax2, count, curr_panel,
                                              False, plot_semilogy)
        # Plot Nyquist frequency values associated with data
        ResolEnhAlgCalib._plot_mtf_nps_nyq_freq(ax1, panel_info, plot_semilogy)
        if num_figs > 1:
            ResolEnhAlgCalib._plot_mtf_nps_nyq_freq(ax2, panel_info, plot_semilogy)
        # Labels and titles
        ax1.set_xlabel('Frequency [cycles/mm]')
        ax1.legend(loc='upper right')
        ax1.set_title("MTF")
        if num_figs > 1:
            ax2.set_xlabel('Frequency [cycles/mm]')
            ax2.legend(loc='upper left')
            ax2.set_title("NPS")
        _plt.tight_layout()

        # need to return fig to make pytest-mpl work correctly
        return fig


    @staticmethod
    def create_encrypted_REA_xml(panel_info, filename=None):
        """
        Create encrypted REA xml text for the given panel information.

        Args:
            panel_info: (list)
            filename: (str)
                    if filename is given, then an appropriate file will
                     be saved

        Returns:

        """

        k_mtf_freq_str = "Freq_cycles_per_mm"
        k_mtf_str = "MTF"
        k_nps_freq_str = "Freq_cycles_per_mm_NPS"
        k_nps_str = "NPS"

        if isinstance(panel_info, list) and len(panel_info) == 1:
            panel_info = panel_info[0]
        # MTF
        keys_in_data = panel_info.keys()
        for required_keys in [k_mtf_freq_str, k_mtf_str]:
            assert(required_keys in keys_in_data)

        # NPS
        # if NPS exists but NPS specific freq sample points do not exist,
        # then use the freq points of the MTF
        freq_nps = None
        if k_nps_str in keys_in_data:
            if panel_info[k_nps_str] is not None and \
                    panel_info[k_nps_freq_str] is None:
                freq_nps = panel_info[k_mtf_freq_str]

        # Encode the freq, MTF, freq, NPS information
        enc_str = generate_encrypted_base64_data(
            _k_encryption_scheme, None,
            panel_info[k_mtf_freq_str], panel_info[k_mtf_str],
            freq_nps, panel_info[k_nps_str])

        # Generate string stream for the xml file
        xml_file_text = ResolEnhAlgCalib._generate_complete_xml_text(
            panel_info, enc_str)

        if filename is not None:
            with open(filename, "w") as fid:
                file_contents = "".join(xml_file_text)
                fid.write(file_contents)
        else:
            for line_txt in xml_file_text:
                _logging.info(line_txt),

        return xml_file_text

    @staticmethod
    def url_to_get_serial_number():
        """URL to point your browser to input the panel type and customer,
           and retrieve specific panel serial numbers.
        """
        return "http://slfpweb2.vms.ad.varian.com:8093/Receptor_Test_Reports"

    @staticmethod
    def decode_encrypted_REA_xml(xml_txt_str):
        """

        Args:
            xml_txt_str: (list of str or a str)

        Returns:
        """
        k_calibration_encryption_field = 'CalibrationData'
        k_start_marker = '<{}>'.format(k_calibration_encryption_field)
        k_end_marker = '</{}>'.format(k_calibration_encryption_field)

        enc_line = []
        if isinstance(xml_txt_str, list):
            for elem in xml_txt_str:
                if k_start_marker in elem:
                    enc_line = elem
                    break
        elif isinstance(xml_txt_str, str):
            enc_line = xml_txt_str

        # Remove \n
        enc_line = enc_line.replace('\n', '')

        # Find <CalibrationData> ... </CalibrationData> format
        pattern = "{}(.*){}".format(k_start_marker, k_end_marker)
        match = _re.search(pattern, enc_line)

        if match:
            # There should be only one MTF / NPS data
            assert(len(match.groups()) == 1)
            enc_str = match.groups()[0]
            # Decrypt and parse the string to extract f, MTF, and NPS
            mtf, nps = recover_data(enc_str, _k_encryption_scheme)

        return mtf, nps

    # --------------------------
    #   Factory MTF retrieval
    # --------------------------

    def retrieve_factory_mtfs(self, panel_serial_numbers):
        """
        Retrieve factory (test) MTFs based on the serial numbers, and
           update the information internally.

        Args:
            panel_serial_numbers: (list)

        Returns:

        """
        if isinstance(panel_serial_numbers, str):
            panel_serial_numbers = [panel_serial_numbers]

        # Reset self._factory_mtfs
        self._mtfs_pulled_from_online = dict()
        for curr_sn in panel_serial_numbers:
            # Print URLs
            url = find_url_for_receptor(curr_sn)
            if url:
                reg = ('^final.*','^pass.*',r'.*Test(%20|_)Data.*','MTF.*')
                links = extract_link_tree(url, reg)
                # If links is a length 1 list, then do away with list
                if isinstance(links, list) and len(links) == 1:
                    links = links[0]

                if links is not None and links:
                    _logging.info("Found MTF file for S/N {} at: {}".format(
                        curr_sn, links))
                    # Get data (using Alan's code)
                    curr_mtf_info = get_factory_mtfs(curr_sn)
                    # If length 1 list, then do away with list
                    if isinstance(curr_mtf_info, list) and len(curr_mtf_info) == 1:
                        curr_mtf_info = curr_mtf_info[0]
                    # Add to self._mtfs_pulled_from_online
                    if curr_mtf_info is not None and curr_mtf_info:
                        self._mtfs_pulled_from_online[curr_sn] = curr_mtf_info
                        # Add the url link
                        self._mtfs_pulled_from_online[curr_sn]['url'] = links
                    _logging.info("Retrieved MTF for panel with S/N {}".format(
                        curr_sn))
            else:
                _logging.info("No valid MTF found for S/N {} found!".format(
                    curr_sn))


# =============================================================================
# vim: tabstop=4 shiftwidth=4 softtabstop=4 expandtab
