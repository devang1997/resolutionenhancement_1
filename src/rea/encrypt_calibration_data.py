#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# XOR based encryption and base64 encoding of sampled frequency, MTF, and NPS
# for REA deployment.

__author__  = "Sungwon Yoon (sungwon.yoon@vareximaging.com)"
__license__ = "Copyright (c) 2013 Varex Imaging"

import numpy as _np
from itertools import cycle
import base64 as _base64
import struct as _struct
import string as _string
import random as _random
import os as _os
# Number of bytes for float32
_k_num_bytes = 4
# Salt key bytes
k_num_salt_bytes = 28
# Global key
_k_key = 'Xhm&g(9N@f[#6Xk>D5d677&T;&^"S3?$^VvA4y%]Nk/6-sGNHf\'#E=f[E?KjTaP\[y+aR/m{qA.jgr5:n75HzM.pfHgEZh@nsA)}%Q~]kvhAbp#s()f$$6)T8?{^TDXA'
# Encryption schemes
k_encryption_schemes = ("xor_base64_salt", "xor_base64", "base64")


def base64_crypt(in_byte_str, encode=False, decode=False):
    """
    Encrypt/decrypt a given string using base64.

    Args:
        in_byte_str: (bytes)
        encode: (bool)
        decode: (bool)

    Returns: (bytes)

    """
    out_str = None
    # Encryption and Decryption
    if decode:
        out_str = _base64.decodebytes(in_byte_str)
    if encode:
        out_str = _base64.encodebytes(in_byte_str)
        #out_str = _base64.encodebytes(in_byte_str).strip()

    return out_str

def xor_crypt_string(in_bytes_str, key_str, salt_str=None,
                     encode=False, decode=False):
    """
    Encrypt/decrypt a given string using xor and base64.

    Args:
        in_bytes_str: (bytes)
        key_str: (str)
        salt_str: (str) when encode == True
                  (bool) when decode == True
        encode: (bool)
        decode: (bool)

    Returns: (bytes)

    """
    if decode:
        in_bytes_str = base64_crypt(in_bytes_str, decode=True)
        # Recover salt from data
        if salt_str:
            salt_str = in_bytes_str[-k_num_salt_bytes:][::-1].decode()
            in_bytes_str = in_bytes_str[:-k_num_salt_bytes]

    # Prepend key with salt
    if salt_str:
        key_str = "".join((salt_str, key_str))
        # Putting the salt after the key is a better choice, because this
        #  scheme will disable finding the correct data length.
        # TODO
        # Need to change the decoding scheme in the REA CST DLL!
        #key_str = "".join((key_str, salt_str))

    # Note that in_str is byte string, but key_str is conventional string
    xored = bytes(x ^ ord(y)
                     for (x,y) in zip(in_bytes_str, cycle(key_str)))

    if encode:
        if salt_str:
            # Append salt to the xored data before base64 encoding
            xored = xored + salt_str[::-1].encode()

        xored = base64_crypt(xored, encode=True)

    return xored


def generate_random_password(length):
    k_random_passwd_len = 1024
    characters = _string.ascii_letters + _string.punctuation + _string.digits
    _random.seed(_os.urandom(k_random_passwd_len))
    password = "".join(_random.choice(characters) for x in range(length))

    return password

def _pack_data(freq_MTF, MTF, freq_NPS, NPS):

    assert (len(freq_MTF) == len(MTF))

    # Method 1
    # # Data length saved as unsigned short
    # part1 = _struct.pack('<H', mtf_len)
    # # 2 float arrays
    # part2 = _struct.pack('<%df' % len(in_data1), *in_data1)
    # part3 = _struct.pack('<%df' % len(in_data2), *in_data2)
    # packed_data = ''.join((part1, part2, part3))
    # #packed_data = _struct.pack('<H', mtf_len)
    # #_struct.pack_into('<%df' % len(in_data), packed_data, 2, *in_data)

    # Method 2
    mtf_len = len(freq_MTF)
    s = _struct.Struct('<2H %df %df' % (mtf_len, mtf_len))
    tmp = tuple([mtf_len, 0]) + tuple(freq_MTF) + tuple(MTF)
    # NPS also provided
    if NPS is not None and freq_NPS is not None:
        nps_len = len(freq_NPS)
        s = _struct.Struct('<2H %df %df %df %df' % (mtf_len, mtf_len, nps_len, nps_len))
        tmp = tuple([mtf_len, nps_len]) + tuple(freq_MTF) + tuple(MTF) + tuple(freq_NPS) + tuple(NPS)

    packed_data = s.pack(*tmp)
    return packed_data

def generate_encrypted_base64_data(crypt_opt, custom_key,
                                   freq_MTF, MTF, freq_NPS=None, NPS=None):
    """
    Generates an encrypted string (XOR and/or Base64 encoding) of the MTF and
    NPS (if requested) along with the sampled frequency points.

    Args:
        crypt_opt: (str)
               'xor_base64_salt' or 'xor_base64' or 'base64'
        custom_key: (str)
                A string that represents a custom key.
                Proviving None will use the default key.
        freq_MTF: (list)
                Sampled frequency points for the MTF
        MTF: (list)
                MTF at the sampled frequency points
        freq_NPS: (list)
                Sampled freq points for the NPS.
                Even if freq_NPS is identical to freq_MTF, it should be
                provided as input.
        NPS: (list)
               NPS at the sampled freq points

    Returns:
        Encrypted string that contains the calibration information, plus "salt"
        used in the decryption.
    """
    packed_data = _pack_data(freq_MTF, MTF, freq_NPS, NPS)

    # Note that the packed_data will only be a byte string!

    # Base64 applied plus encryption
    # http://passwordsgenerator.net/
    # Add salt to data (28 bytes!)
    salt = None
    if crypt_opt == k_encryption_schemes[0]:
        salt = generate_random_password(k_num_salt_bytes)
    # For 'xor_base64_plus_salt' and 'xor_base64'
    if crypt_opt in k_encryption_schemes[:2]:
        # Use default key if nothing is provided by the user
        if custom_key is None:
            custom_key = _k_key
        b64_data = xor_crypt_string(packed_data, custom_key, salt, encode=True)
    # For 'base64'
    elif crypt_opt == k_encryption_schemes[2]:
        b64_data = base64_crypt(packed_data, encode=True)
    else:
        raise RuntimeWarning(_encryption_choice_warning(crypt_opt))

    return b64_data

def recover_data(encrypted_base64_str, crypt_opt=k_encryption_schemes[0],
                 custom_key=None):
    """
    Given an encrypted string, returns a decrypted string.
    An appropriate decoding schemes are 'xor_base64' or 'base64'.

    Args:
        encrypted_base64_str: (str)
        crypt_opt: (str)
        custom_key: (str)
                User supplied custom key string

    Returns: (str)
        Decrypted string
    """
    decrypted_data = [None, None]

    if crypt_opt in k_encryption_schemes[:2]:
        if custom_key is None:
            custom_key = _k_key
        decrypted_data = _recover_data(encrypted_base64_str,
                                       crypt_opt, custom_key)
    elif crypt_opt == k_encryption_schemes[2]:
        decrypted_data = _recover_data(encrypted_base64_str, crypt_opt, None)
    else:
        raise RuntimeWarning(_encryption_choice_warning(crypt_opt))

    return decrypted_data

def _recover_data(encrypted_base64_str, crypt_opt, key):

    k_num_bytes_for_size = 4

    # Salt is bool for decoding
    use_salt = False
    if crypt_opt == k_encryption_schemes[0]:
        use_salt = True
    # Decode base64 with xor
    if key:
        unpacked_data = xor_crypt_string(encrypted_base64_str.encode(),
                                         key, use_salt, decode=True)
    # Decode straight base64
    else:
        unpacked_data = base64_crypt(encrypted_base64_str.encode(),
                                     decode=True)

    # Unpack
    # MTF length, NPS length
    recovered_len = _struct.unpack('<2H', unpacked_data[:k_num_bytes_for_size])
    # Total number of bytes for the actual data
    data_len = len(unpacked_data) - k_num_bytes_for_size
    # Total number of elements should be an integer
    if data_len % (_np.sum(recovered_len) * _k_num_bytes):
        raise RuntimeError("Unable to decode the input data!")
        return [], recovered_len

    # Number of data sets
    num_sets = data_len // (_np.sum(recovered_len) * _k_num_bytes)
    # Recover the correct amount of data
    recovered_data = _np.array(_struct.unpack(
        '<%df' % (num_sets * _np.sum(recovered_len)),
        unpacked_data[k_num_bytes_for_size:]), dtype=_np.float)

    # Take care of MTF
    num_elems = recovered_len[0]
    mtf_data = {'cycles_per_mm': recovered_data[:num_elems],
                'spectrum': recovered_data[num_elems:2*num_elems]}
    # Take care of NPS
    nps_data = None
    if num_sets > 1:
        offset = 2 * num_elems
        num_elems = recovered_len[1]
        nps_data = {
            'cycles_per_mm': recovered_data[offset:offset + num_elems],
            'spectrum': recovered_data[offset + num_elems:offset + 2*num_elems]
        }

    return mtf_data, nps_data

def _encryption_choice_warning(crypt_opt):
    return "Encryption option {} is not recognized. Choice can be either {}, "
    "{}, or {}.".format(crypt_opt, k_encryption_schemes[0],
                        k_encryption_schemes[1], k_encryption_schemes[2])

# =============================================================================
# vim: tabstop=4 shiftwidth=4 softtabstop=4

