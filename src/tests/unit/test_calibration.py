import os
import sys
k_curr_file_path = os.path.dirname(os.path.realpath(__file__))
k_src_path = os.path.realpath(os.path.join(k_curr_file_path, "../../"))
sys.path.append(k_src_path)
import rea
import pytest
import numpy as np
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

k_panel_table_name = "PANEL_MTF_NPS"
k_panel_table_starting_size = 17
k_customer_table_name = "CUSTOMERS"
k_customer_table_starting_size = 5

@pytest.fixture(scope="module")
def calib():
    k_test_sqlite3_file = "{}/../test_panel_info.sqlite".format(
        k_curr_file_path)
    return rea.calibration(k_test_sqlite3_file)
    #return rea.calibration()


@pytest.fixture(scope="module")
def test_panel():
    panel_info = {
        "Panel_type": "test_panel_nonexisting",
        "Serial_num": "12345^&*()",
        "Scintillator": "magic_material",
        "Pixel_size_mm": 0.111,
        "Freq_cycles_per_mm": np.random.rand(100),
        "MTF": np.random.rand(100),
        "Freq_cycles_per_mm_NPS": np.random.rand(71),
        "NPS": np.random.rand(71),
        "Notes": "Bogus panel info for testing purposes only."
    }
    return panel_info


def test_init(calib):
    assert(calib._cursor)
    assert(calib._db_connect)
    assert(calib._sqlite3_file)
    assert(not calib._mtfs_pulled_from_online)
    assert(calib._table_info)


def test_query(calib):
    query_str = "SELECT * FROM {}".format(calib._sqlite3_file.mtf_nps)
    result = calib.query(query_str)
    assert(len(result) > 0)


def test_num_MTFs(calib):
    print("Number of MTFs: {}".format(calib.num_MTFs()))
    assert(calib.num_MTFs() == k_panel_table_starting_size)


def test_num_customers(calib):
    print("Number of customers: {}".format(calib.num_customers()))
    assert(calib.num_customers() == k_customer_table_starting_size)


def test_get_panel_with_id(calib):
    info = calib.get_panel_with_id(11)
    assert(not info)
    info = calib.get_panel_with_id(100)
    assert(not info)
    info = calib.get_panel_with_id(9)
    assert(info['Panel_type'] == "3030D")
    assert(info['Serial_num'] == "E618-07")
    assert(info['Scintillator'] == "CsI (600um)")
    assert(info['Pixel_size_mm'] == 0.194)
    assert("3030D CBM Rev AD 33832" in info['Notes'])


def test_get_columns_of_table(calib):
    tables = calib.get_list_tables()
    assert(k_panel_table_name in tables)
    assert(k_customer_table_name in tables)


@pytest.mark.parametrize("serial_num, expected_result",
                         [("349S05-0901", True),
                          ("238S01-0703", True),
                          ("PKI_0822AP5", False)])
def test_get_panel_with_serial_num(calib, serial_num, expected_result):
    panel_info = calib.get_panel_with_serial_num(serial_num)
    if expected_result:
        assert(isinstance(panel_info, dict))
    else:
        assert(panel_info is None)


# Trick to pass keyword arguments: use dict as argument, and use **argument
@pytest.mark.parametrize(
    "arg, expected",
    [
        ({"wrong_keyword": "noMatch"}, None),
        ({"panel_type": "2530HE"},
         {"Serial_num": ["235S06-1003", "238S01-0703", None, "540S04-2003","243S01-0103"]}),
        ({"Scintillator": "CsI"},
         {"Serial_num": ["E618-07", "310S01-1305", "438S06-1904", "243S01-0103", "MTF_on_det", "MTF_at_pan_focus", "PKI_0822AP3"]})
    ])
def test_get_panel_that_have(calib, arg, expected):
    panel_info = calib.get_panels_that_have(**arg)

    if expected is None:
        assert(not panel_info)
    else:
        # Make sure the length is correct
        assert(len(panel_info) == len(list(expected.values())[0]))
        # Check the retrieved panel S/N
        for elem in panel_info:
            for key, val in expected.items():
                assert(elem[key] in val)


@pytest.mark.parametrize("id, expected_vals",
                         [(1, ("Imaging", 53)),
                          (7, ("Varex", 9))])
def test_get_customer_with_id(calib, id, expected_vals):
    customer_info = calib.get_customer_with_id(id)
    assert(customer_info['xml_id'] == id)
    assert(customer_info['Name'] == expected_vals[0])
    assert(customer_info['Panel_id'] == expected_vals[1])


@pytest.mark.parametrize("name, panel_ids, xml_ids",
                         [("Varex", (9, 16), (7, 4)),
                          ("Imaging", (53, 54), (1, 2)),
                          ("Startup", 44, 8)])
def test_get_customers_with_name(calib, name, panel_ids, xml_ids):
    customers = calib.get_customers_with_name(name)
    if isinstance(customers, list):
        assert(len(customers) == len(panel_ids))
        for elem in customers:
            # loop through list does not guarantee ordering,
            #  so find the correct index
            k = [idx for idx in range(len(panel_ids))
                 if panel_ids[idx] == elem['Panel_id']][0]
            assert(elem['Panel_id'] == panel_ids[k])
            assert(elem['xml_id'] == xml_ids[k])
    else:
        assert(isinstance(customers, dict))
        assert(customers['Panel_id'] == panel_ids)
        assert(customers['xml_id'] == xml_ids)


# To use, you simply need to mark the function where you want to compare
#  images using @pytest.mark.mpl_image_compare, and make sure that the function
#  returns a Matplotlib figure (or any figure object that has a savefig method)
#
# To generate the baseline images, run the tests with the --mpl-generate-path
#  option with the name of the directory where the generated images should be
#  placed:
#       py.test --mpl-generate-path=ref_plots
# If the directory does not exist, it will be created. The directory will be
#  interpreted as being relative to where you are running py.test. Once you are
#  happy with the generated images, you should move them to a sub-directory
#  called baseline relative to the test files (this name is configurable,
#  see below). You can also generate the baseline images directly in the right
#  directory.
# You can then run the tests simply with:
#       py.test --mpl
@pytest.mark.mpl_image_compare(baseline_dir='../ref_plots')
@pytest.mark.parametrize("plot_semilogy", (False, True))
def test_plot_MTF_NPS(calib, plot_semilogy):
    k_customer_name = "Varex"
    k_serial_num = "PKI"

    if plot_semilogy:
        customer_info = calib.get_customers_with_name(k_customer_name)

        panel_info = list()
        for curr_customer in customer_info:
            curr_id = curr_customer["Panel_id"]
            panel_info.append(calib.get_panel_with_id(curr_id))
        # Semilogy
        fig = calib.plot_MTF_NPS(panel_info, plot_semilogy)

    else:
        panel_info = calib.get_panels_that_have(serial_num=k_serial_num)
        fig = calib.plot_MTF_NPS(panel_info, plot_semilogy)

    return fig


def test_url_to_get_serial_number(calib):
    url = calib.url_to_get_serial_number()
    assert("Test_Reports" in url)


@pytest.mark.parametrize("args, include_nps",
                         [
                             ({"Panel_id": 42}, False),
                             ({"Serial_num": "PKI_0822AP3"}, True),
                             ({"Panel_id": 58}, False)
                         ])
def test_encrpyt_decrypt_REA_xml(calib, args, include_nps):
    """
    Combined testing for functions create_encrypted_REA_xml and
     decode_encrypted_REA_xml.

    :NOTE:
    Heavy-lifting encoding and decoding tests are done in
     the testing of encrypt_calibration_data module (i.e.,
     test_encrypt_calibration_data.py)
    """
    panel = calib.get_panels_that_have(**args)

    output_xml_file = os.path.join(
       k_curr_file_path, "rea_encrypted_file_for_testing.xml"
    )
    if not include_nps and len(panel):
        panel[0]['NPS'] = None
    # List to dict
    panel = panel[0]

    # Encrypted xml returned and also written out as file
    try:
        panel_enc_xml = calib.create_encrypted_REA_xml(panel, output_xml_file)
        # Check that xml file exists
        assert(os.path.isfile(output_xml_file))
        # Check the content
        with open(output_xml_file, 'rt') as file:
            read_in_txt = file.read()
            assert(read_in_txt == ''.join(panel_enc_xml))
        # Decode the encrypted
        mtf, nps = calib.decode_encrypted_REA_xml(panel_enc_xml)
        # Compare to reference
        assert(np.allclose(panel["Freq_cycles_per_mm"], mtf["cycles_per_mm"]))
        assert(np.allclose(panel["MTF"], mtf["spectrum"]))
        if include_nps:
            freq_nps_key = "Freq_cycles_per_mm_NPS"
            if panel["Freq_cycles_per_mm_NPS"] is None:
                freq_nps_key = "Freq_cycles_per_mm"
            assert(np.allclose(panel[freq_nps_key], nps["cycles_per_mm"]))
            assert(np.allclose(panel["NPS"], nps["spectrum"]))
        else:
            assert(not nps["cycles_per_mm"])
            assert(not nps["spectrum"])

    finally:
        # Delete temporary xml file
        os.remove(output_xml_file)


# Changes to the DB file
# ---------------------------------

def test_add_delete_panel_info(calib, test_panel):
    """
    Combined testing for functions add_new_panel_info and
     delete_panel_info_with_serial_num.

    """
    try:
        # Insert a new item
        calib.add_new_panel(test_panel["Serial_num"],
                            test_panel["Panel_type"],
                            test_panel["Scintillator"],
                            test_panel["Pixel_size_mm"],
                            test_panel["Freq_cycles_per_mm"],
                            test_panel["MTF"],
                            freq_nps=test_panel["Freq_cycles_per_mm_NPS"],
                            nps=test_panel["NPS"],
                            notes=test_panel["Notes"])
        # Retrieve the inserted panel from DB
        inserted_panel = calib.get_panel_with_serial_num(
            test_panel["Serial_num"])
        # Compare values against reference
        for key, val in inserted_panel.items():
            if val and key not in "Panel_id":
                # Check per element for lists
                if isinstance(val, list):
                    assert(np.allclose(test_panel[key], val))
                else:
                    assert(test_panel[key] == val)
    finally:
        # Delete the inserted item
        calib.delete_panel_with_serial_num(test_panel["Serial_num"])


def test_record_calib_generation(calib):
    k_test_panel_id = 42
    panel = calib.get_panel_with_id(k_test_panel_id)

    try:
        # Add record in the CUSTOMERS table
        new_record = calib.record_calib_generation(
            panel['Panel_id'], "Ghost customer for testing purposes")
        # Separately query the CUSTOMERS table for the row with the panel id
        queried_record = calib.get_customers_with_panel_id(panel['Panel_id'])
        # Check that what's been inserted is the same as what we started with
        for key, val in new_record.items():
            assert(queried_record[key] == val)
    finally:
        # Delete the test customer record at the very end
        calib.delete_customer_with_id(new_record['xml_id'])


@pytest.mark.parametrize("panel_ids",
                         [([40]),
                          ([9, 40])])
def test_retrieve_factory_mtfs(calib, panel_ids):
    # Generate a list of serial numbers
    serial_num_list = list()
    for elem in panel_ids:
        panel_info = calib.get_panel_with_id(elem)
        serial_num_list.append(panel_info["Serial_num"])
    # Retrieve panel info from online for the specified serial numbers
    calib.retrieve_factory_mtfs(serial_num_list)

    for serial_num, curr_panel in calib.mtfs_pulled_from_online.items():
        # Fetch panel information in the PANEL_MTF_NPS table
        panel_info = calib.get_panel_with_serial_num(serial_num)
        # Check the serial number
        assert(panel_info["Serial_num"] == serial_num)
        for key, val in curr_panel.items():
            if key in panel_info:
                # Check other parameters
                if isinstance(val, list):
                    assert(np.allclose(panel_info[key], val))
                else:
                    if 'Panel_type' in key:
                        assert(panel_info[key] in val or 'mode_1_MTF' in val)
                    else:
                        assert(panel_info[key] == val)


def test_retrieve_factory_mtfs_bad_serial(calib):
    # Non-existent serial numbers
    serial_num_list = ("676S06-2001", "681S04-2001")
    # Retrieve panel info from online for the specified serial numbers
    calib.retrieve_factory_mtfs(serial_num_list)
    # There should be nothing pulled from online, while there should be no
    #  interruption to the code
    assert(not calib.mtfs_pulled_from_online)


def test_add_new_panel_from_online(calib):

    k_test_report_id = "3030DX-I YXN Rev AC 82955"
    k_manually_identified_serial_nums = (
        "626S06-2002", "601S04-2002"
    #    "626S06-2002", "601S05-1803"
    #    "601S04-2004", "629S10-1802",
    #    "535S07-1404", "624S13-2002", "601S04-2002"
    )

    # Make sure the new serial numbered panel info are not available
    for curr_serial_num in k_manually_identified_serial_nums:
        panel_info = calib.get_panel_with_serial_num(curr_serial_num)
        assert(panel_info is None)

    try:
        # Fetch MTF information based on the serial numbers and update the database
        calib.add_new_panel_from_online(k_test_report_id,
                                        k_manually_identified_serial_nums,
                                        "3030DX-I", "Scintillator Test Only")
        # Make sure the size is increased by 2
        assert(calib.num_MTFs() == k_panel_table_starting_size + len(k_manually_identified_serial_nums))
        # Make sure newly added data is available
        for curr_serial_num in k_manually_identified_serial_nums:
            panel_info = calib.get_panel_with_serial_num(curr_serial_num)
            # Should be 1 panel info returned as dict
            assert(isinstance(panel_info, dict))
            # Note section should match the report id
            assert(panel_info["Notes"] == k_test_report_id)
    finally:
        # Remove added panel information
        for curr_serial_num in k_manually_identified_serial_nums:
            calib.delete_panel_with_serial_num(curr_serial_num)
    # Now, the total number of rows should equal k_starting_size
    assert(calib.num_MTFs() == k_panel_table_starting_size)


