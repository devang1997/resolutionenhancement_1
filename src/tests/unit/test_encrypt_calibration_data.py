import os
import sys
curr_file_path = os.path.dirname(os.path.realpath(__file__))
src_path = os.path.realpath(os.path.join(curr_file_path, "../../rea"))
sys.path.append(src_path)
from encrypt_calibration_data import *
import numpy as np
import pytest

@pytest.fixture
def orig_inputs():
    # Arbitrary data length
    in_data = list()
    in_data.append(np.random.rand(11) * 100)
    in_data.append(np.random.rand(11) * 200)
    in_data.append(-np.random.rand(7) * 100)
    in_data.append(-np.random.rand(7) * 200)
    return in_data

def encrypt_sample(orig_inputs, crypt_opt, custom_key=None):
    """
    Args:
        crypt_opt: (str)
                'xor_base64' or ''base64'
                When 'xor_base64', mtf and nps are encoded
                When 'base64', only the mtf is encoded. Thus, no nps.
        custom_key: (str)
                Custom key, if user desires
    Returns: (bytes)
                Encrypted bytes string

    """
    if crypt_opt in k_encryption_schemes[:2]:
        b64 = generate_encrypted_base64_data(
            crypt_opt, custom_key, orig_inputs[0], orig_inputs[1],
            orig_inputs[2], orig_inputs[3])
    elif crypt_opt == k_encryption_schemes[2]:
        b64 = generate_encrypted_base64_data(
           crypt_opt, custom_key, orig_inputs[0], orig_inputs[1])

    return b64

def decrypt_sample(encrypted_str, crypt_opt, custom_key=None):
    """
    Args:
        encrypted_str: (str)
        crypt_opt:  (str)
            'xor_base64' or 'base64'
        custom_key: (str)
            None will fall back on to the default key
    """
    if crypt_opt in k_encryption_schemes[:2]:
        mtf, nps = recover_data(encrypted_str, crypt_opt, custom_key)
    elif crypt_opt == k_encryption_schemes[2]:
        mtf, nps = recover_data(encrypted_str, crypt_opt)

    return mtf, nps

@pytest.mark.parametrize("passwd_len", [28, 256, 512, 1024])
def test_generate_random_password(passwd_len):
    # Check that the password has correct length
    password = generate_random_password(passwd_len)
    assert(len(password) == passwd_len)

def test_base64_encrypt():
    in_bytes = b"thisIsaRandomByteString!@#$%^&*)(-=+}{><?"
    # Encode
    out_bytes = base64_crypt(in_bytes, encode=True)
    # Check that the output is bytes object and the length has not shrunk
    assert(isinstance(out_bytes, bytes))
    assert(len(out_bytes) >= len(in_bytes))
    # Decode
    decoded_bytes = base64_crypt(out_bytes, decode=True)
    # Check that we recover the original bytes string
    assert(isinstance(decoded_bytes, bytes))
    assert(in_bytes == decoded_bytes)

@pytest.mark.parametrize(
    "key_str, use_salt",
    [("!@#$%random^&*()_+-=key;][\/.,><83ktoea8-43st34tmhq", False),
     ("!@#$%random^&*()_+-=key;][\/.,><", True)])
def test_xor_crypt_string(key_str, use_salt):
    salt_str = None
    if use_salt:
        salt_str = generate_random_password(k_num_salt_bytes)
    in_bytes = "+_)-=897Another?<>}{!@#random_#$@%%&*^input".encode()
    # Encode
    out_bytes = xor_crypt_string(in_bytes, key_str, salt_str, encode=True)
    # Check that the output is bytes object and the length has not shrunk
    assert(isinstance(out_bytes, bytes))
    assert(len(out_bytes) >= len(in_bytes))
    # Decode
    decoded_bytes = xor_crypt_string(out_bytes, key_str, salt_str, decode=True)
    # Check that we recover the original bytes string
    assert(isinstance(decoded_bytes, bytes))
    assert(in_bytes == decoded_bytes)

@pytest.mark.parametrize(
    "crypt_opt, custom_key",
    [(k_encryption_schemes[2], None),
     (k_encryption_schemes[1], None),
     (k_encryption_schemes[1], '8[FhZ<Qy;U c_?"lwRFP Lcm_WD8E'),
     (k_encryption_schemes[0], None),
     (k_encryption_schemes[0], '8[FhZ<Qy;U c_?"lwRFP Lcm_WD8E')])

def test_recover_data(orig_inputs, crypt_opt, custom_key):
    """
    Verify the decrypted result is as expected.
    """
    b64 = encrypt_sample(orig_inputs, crypt_opt, custom_key)
    # Note that b64 is a bytes string.
    # decrypt_sample requires a str string as input, thus the use of decode()
    enc_str = b64.decode()
    mtf, nps = decrypt_sample(enc_str, crypt_opt, custom_key)

    assert(np.allclose(orig_inputs[0], mtf['cycles_per_mm']))
    assert(np.allclose(orig_inputs[1], mtf['spectrum']))
    if crypt_opt in k_encryption_schemes[:2]:
        assert(np.allclose(orig_inputs[2], nps['cycles_per_mm']))
        assert(np.allclose(orig_inputs[3], nps['spectrum']))
    else:
        # nps should have all None values
        assert(not nps['cycles_per_mm'])
        assert(not nps['spectrum'])


@pytest.mark.xfail(raises=RuntimeError, #strict=True,
                   reason="k_encryption_schemes[0] passes because the internal "
                          "key is (salt + custom_key), where salt is fixed")
@pytest.mark.parametrize(
    "crypt_opt, custom_key",
    [(k_encryption_schemes[1], None),
     (k_encryption_schemes[1], '8[FhZ<Qy;U c_?"lwRFP Lcm_WD8E'),
     (k_encryption_schemes[0], None),
     (k_encryption_schemes[0], '8[FhZ<Qy;U c_?"lwRFP Lcm_WD8E')])
def test_recover_data_with_incorrect_key(orig_inputs, crypt_opt, custom_key):

    k_bogus_key = 'UlcZ8PE[mwR;Qc<_LW"? hDFF8 y_'
    b64 = encrypt_sample(orig_inputs, crypt_opt, custom_key)
    # Note that b64 is a bytes string.
    # decrypt_sample requires a str string as input, thus the use of decode()
    enc_str = b64.decode()
    mtf, nps = decrypt_sample(enc_str, crypt_opt, k_bogus_key)

    assert(not np.allclose(orig_inputs[0], mtf['cycles_per_mm']))
    assert(not np.allclose(orig_inputs[1], mtf['spectrum']))
    assert(not np.allclose(orig_inputs[2], nps['cycles_per_mm']))
    assert(not np.allclose(orig_inputs[3], nps['spectrum']))
    # TODO
    # In the future, when key + salt is finalized in the CST REA DLL
    # with pytest.raises(RuntimeError):
    #     mtf, nps = decrypt_sample(enc_str, crypt_opt, k_bogus_key)
