\chapter{Wiener Deconvolution}
Wiener deconvolution is the general model we adopt for the Resolution Enhancement Algorithm (REA). Therefore, Wiener deconvolution algorithm is re-visited.

\section{Derivation}

Assume a linear model
\begin{equation}
  y(t) = h(t) \Conv x(t) + n(t),
  \label{eqn:linearModel}
\end{equation}
where $y(t)$ is the observed value, $h(t)$ is the system response function, and $n(t)$ is the additive noise. The goal is to find some $g(t)$ such that we can estimate $x(t)$ as follows:
\begin{equation}
  \hat{x}(t) = g(t) \Conv y(t),
  \label{eqn:estimateX}
\end{equation}
where $\hat{x}$ is an estimate of $x$ that minimizes the mean squared error. Mathematically, it can be expressed as
\begin{equation}
  \min_{G(f)} \mathcal{E}(f)  = \mathrm{E}\left[ |X(f) - \widehat{X}(f)|^2 \right],
  \label{eqn:quadraticCost}
\end{equation}
where $\widehat{X}(f) = G(f) Y(f)$.

The quadratic cost function can be written as
\begin{align}
  \mathcal{E}(f) &= \mathrm{E}\left[ |X(f) - G(f)Y(f)|^2 \right], \nonumber \\
				 &= \mathrm{E}\left[ X^*(f)X(f) - 2 G^*(f)Y^*(f)X(f) + G^*(f)Y^*(f)Y(f)G(f) \right], \nonumber \\
				 &= \mathrm{E}\big[ X^*(f)X(f) - 2 G^*(f)\left(X^*(f)H^*(f)+ N^*(f)\right)X(f) \nonumber \\
				 & \quad \quad + G^*(f)\left(X(f)H(f) + N(f)\right)^* \left(X(f)H(f)+N(f)\right)G(f) \big].
  \label{eqn:expandedQuadraticCost}
\end{align}
By setting the derivative with respect to $G(f)$ to zero ($\frac{\partial \mathcal{E}(f)}{\partial G(f)}  = 0$),
\begin{equation*}
  \mathrm{E}\left[ |X(f)|^2 H(f) + X(f) N^*(f) \right] = \mathrm{E}\left[ \left( |X(f)|^2 |H(f)|^2 + 2 H^*(f) X^*(f) N(f) + |N(f)|^2 \right) G(f) \right],
\end{equation*}
we can find $G(f)$ that minimizes (\ref{eqn:quadraticCost}).

We assume that the noise $n(t)$ is independent of the signal $x(t)$, \textit{i.e.}, $\mathrm{E}[ X^*(f) N(f) ] = 0$, which results in
\begin{equation}
  \mathrm{E}\left[|X(f)|^2\right] H(f) = \left(\mathrm{E}\left[|X(f)|^2\right] |H(f)|^2 + \mathrm{E}\left[|N(f)|^2 \right] \right) G(f).
  \label{eqn:derivativeEqualsZero}
\end{equation}
By defining the signal and noise power spectral densities,
\begin{align*}
  S_X(f) = \mathrm{E}\left[ |X(f)|^2 \right], \\
  S_N(f) = \mathrm{E}\left[ |N(f)|^2 \right],
\end{align*}
we have the following result as the deconvolution kernel (in the frequency domain)
\begin{align}
  G(f) &= \frac{S_X(f) H(f)}{S_X(f) |H(f)|^2 + S_N(f)}, \\
	&= \frac{H(f)}{|H(f)|^2 + \frac{S_N(f)}{S_X(f)}}.
  \label{eqn:deconvKernel}
\end{align}
If we assume no knowledge about the noise characteritics, we can set $S_N(f) = 0$, which results in the simple deconvolution via system MTF division, \textit{i.e.}, $G(f) = 1 / H(f)$. By having some knowledge of the noise characteristics, we can prevent unwanted noise amplification.

\section{Modified model used in CST}\label{sec:modifiedWienerDeconvModel}
We assume that the system MTF $H(f)$, signal PSD $S_X(f)$, and noise PSD $S_N(f)$ are measured and parameterized to be used by the CST REA plugin. We have decided to provide 2 run-time parameters that can alter the deconvolution kernel $G(f)$ without having to re-model the system MTF, signal PSD, and noise PSD. First parameter $\alpha$ controls the strength of the deconvolution kernel and is interfaced via the parameter \texttt{correction\_strength}. The parameter $\alpha$ is modeled as
\begin{equation}
  H^\prime_{\alpha} (f) = (1 - \alpha) + \alpha H(f), \quad \quad \alpha \in [0, 1].
  \label{eqn:Hprime}
\end{equation}
Note that if $\alpha = 1$, then $H^\prime_{\alpha}(f) = H(f)$ and the correction is at full strength. In contrast, if $\alpha = 0$, then $H^\prime_{\alpha}(f) = 1$ for all $f$; therefore, $h^\prime_{\alpha}(t) = \delta(t)$, and there is no correction applied.

The second parameter, $\gamma \in [0, 1]$, empirically controls the confidence of the ratio of noise PSD over signal PSD ($\frac{S_N(f)}{S_X(f)}$). In other words, (\ref{eqn:deconvKernel}) can be rewritten as
\begin{equation}
  W(f; \alpha, \gamma) = \frac{[H^\prime_{\alpha}(f)]^*}{|H^\prime_{\alpha}(f)|^2 + \gamma \; \frac{S_N(f)}{S_X(f)}}.
  \label{eqn:modifiedDeconvKernel}
\end{equation}
Note that $\gamma = 1$ indicates full confidence in the value for the ratio of noise PSD over signal PSD; whereas, $\gamma = 0$ disregards any knowledge of the noise/signal PSD. The parameter $\gamma$ is interfaced via the parameter \texttt{noise\_PSD\_strength} in the REA plugin.

\section{Further model modification used in CST}
\subsection{\texttt{MtfAdjustmentFactor}}
\begin{figure}
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figs/H_arith_geom}
		\caption{$H^\prime_\alpha(f)$ using arithmetic weighting (\textit{dotted line}) and geomtric weighting (\textit{solid line}). Note that the two weighting schemes produce same results for $\alpha=0$ and $\alpha=1$.}
		\label{fig:modified_MTFs_1313DX}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=0.94\textwidth]{figs/Hwiener_MtfAdjFactor}
		\caption{$W(f; \alpha, G_{target})$ as a function of $\alpha^{-1}$ (\texttt{MtfAdjustmentFactor}). Note that the band-pass region shifts to higher frequency as \texttt{MtfAdjustmentFactor} increases.}
		\label{fig:Wiener_filter_function_of_alpha}
	\end{subfigure}
	\caption{$H^\prime_\alpha(f)$ and $W(f\:;\: \alpha, G_{target})$ as a function of $\alpha$ values. }
\end{figure}
The parameter \texttt{correction\_strength} has been changed to \texttt{MtfAdjustmentFactor}. Instead of using the arithmetic weighting model in (\ref{eqn:Hprime}), we use the following geometric weighting model.
\begin{equation}
	H^\prime_{\alpha} (f) = 1^{1-\alpha} + H^\alpha(f) = H^\alpha(f), \quad \quad \alpha \geq 0,
\end{equation}
and \texttt{MtfAdjustmentFactor} $= \alpha^{-1}$.
The new geometric weighting and the previous arithmetic weighting results are equal for values $\alpha = 0$ and $\alpha = 1$, but differ for all values in between. The new definition (geometric weighting) allows the psf kernel to have a narrower shape in the frequency domain when $\alpha > 1$, which corresponds to increased blurring in the spatial domain. As the $\alpha$ value transitions from 1 to 0, geometric weighting widens $H^\prime(f)$ in the frequency domain (narrower blurring kernel width in spatial domain) from the original $H(f)$ to a constant value. Fig.\ref{fig:modified_MTFs_1313DX} illustrates a comparison of the modified MTFs ($H^\prime_\alpha(f)$) for the 1313DX panel. Instead of using \texttt{MtfAdjustmentFactor} = $\alpha$, we adopted the definition \texttt{MtfAdjustmentFactor} = $\alpha^{-1}$ to maintain the intuitive direction of the parameter: greater \texttt{MtfAdjustmentFactor} value indicates an attempt to make the correction "sharper." Fig.\ref{fig:Wiener_filter_function_of_alpha} illustrates the correction filter $W(f\:;\: \alpha, G_{target})$ changing shape as a function of $\alpha$, with fixed $G_{target} = G_6$ (in Fig.\ref{fig:Wiener_filter_function_of_G}).

\subsection{\texttt{GainAmplificationLimitDb}}
In the new model, the ratio of noise PSD over signal PSD in (\ref{eqn:modifiedDeconvKernel}) has been lumped into a single parameter $R(f)$. Therefore, (\ref{eqn:modifiedDeconvKernel}) can be written as
\begin{equation}
	W(f; \alpha, \gamma) = \frac{[H^\prime_{\alpha}(f)]^*}{|H^\prime_{\alpha}(f)|^2 + \gamma \; R(f)}, \quad \gamma \geq 0.
	\label{eqn:newModifiedDeconvKernel}
\end{equation}
Note that since we assume a symmetric detector psf, the Fourier transform of the psf $H(f)$ and its variants are all real numbers; thus, we drop the conjugate operator $^*$ from hereon.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{figs/Hwiener_GainAmpLimit_1}
		\caption{$W(f\:;\: \alpha, G_{target} = G_i)$ for lower $G_i$.}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figs/Hwiener_GainAmpLimit_2}
		\caption{$W(f\:;\: \alpha, G_{target} = G_i)$ for higher $G_i$.}
	\end{subfigure}
	\caption{$W(f\:;\: \alpha=1, G_{target} = G_i)$ as a function of $G_i$ values. Note that $G_i > G_j$ for $\forall i > j$. Reference values are $G_1$ = 0dB and $G_7$ = 10dB.}
	\label{fig:Wiener_filter_function_of_G}
\end{figure}

Additionally, the variable $\gamma$ is not explicitly exposed to the user. This parameter is determined internally as a function of the user provided parameter \texttt{GainAmplificationLimitDb} ($G_{target} = 10^{\left(\textrm{\texttt{GainAmplificationLimitDb}} / 10\right)}$), which is the desired upper bound of noise amplification level in decibel units. To find the appropriate $\gamma$ value for a given $G_{target}$, we solve the following problem.
\begin{equation}
	%\frac{H^\prime_{\alpha}(f)}{|H^\prime_{\alpha}(f)|^2 + \gamma \; R(f)} \leq G_{target},\quad\quad \forall f \in \tilde{S} = \left\{f\quad | \quad \frac{H^\prime_{\alpha}(f)}{R(f)} < T_{SNR} \right\}.
	\frac{H^\prime_{\alpha}(f)}{|H^\prime_{\alpha}(f)|^2 + \gamma \; R(f)} \leq G_{target},\quad\quad \forall f \in \tilde{S} = \left\{f\quad | \quad 0 \leq f \leq \frac{f_{Nyquist}}{2} \right\}.
	\label{eqn:upper_limit_gain}
\end{equation}
Therefore, the optimal $\gamma$ can be computed as
\begin{equation}
	\widehat{\gamma}\left(G_{target}\right) =  \max\left\{0,\quad \max\limits_{f \in \tilde{S}}\left\{ \frac{H^\prime_{\alpha}(f)}{R(f)} \left[\frac{1}{G_{target}} - H^\prime_{\alpha}(f)\right] \right\} \right\}.
\end{equation}
Note that $\gamma \geq 0$ and therefore $\widehat{\gamma} \geq 0$. From (\ref{eqn:upper_limit_gain}), it makes sense to have $G_{target} \geq 0$, but the implemented algorithm will not restrict the user from using a negative value for $G_{target}$.



Fig.\ref{fig:Wiener_filter_function_of_G} shows $W(f;\, \alpha = 1.0, \, G_{target})$ for different values of $G_{target} = G_i$ (dB), assuming a constant $R(f)$. Large $G_{target}$ values result in Wiener filter that is closer to $1/H^\prime(f)$. Small $G_{target}$ values result in stronger suppression of noise in the high frequency region, and has the effect of shifting the band-pass region closer to the DC.




