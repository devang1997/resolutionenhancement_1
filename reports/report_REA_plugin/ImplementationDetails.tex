\chapter{Implementation details}
\section{Calibration measurements}
Calibration measurements consist of
\begin{enumerate}
  \item Sampled frequency values for $H(f)$: $f_k = k \: \Delta f_{sampled}, \; k = 0, 1, \cdots, K-1$
  \item Sampled detector MTF values: $H(f_k), \; k=0, 1, \cdots, K-1$
  \item Sampled frequency values for $R(f)$: $ f_m = m \: \Delta \tilde{f}_{sampled}, \; m = 0, 1, \cdots, M-1$
  \item Sampled regularizer: $R(f_m), \; m = 0, 1, \cdots, M-1$
  \begin{itemize}
    \item Sampled noise power spectral density (PSD) and input PSD: $R(f_m) = S_N(f_m) / S_{in}(f_m)$
    \item Sampled noise power spectral density (PSD) and input variance: $R(f_m) = \frac{S_N(f_m)}{ \sigma_{in}^2 / 2 / f_{Nyquist}}$
    \item Noise variance and input PSD: $R(f_m) = \frac{\sigma_N^2 / 2 / f_{Nyquist}}{S_{in}(f_m)}$
    \item Noise variance and input variance: $R(f_m) = \frac{\sigma_N^2}{\sigma_{in}^2}$
  \end{itemize}
\end{enumerate}
Sampled frequency and sampled detector MTF values are required input measurements for an effective REA. For wider spatial detector blurring kernel, some knowledge of the noise characteristics can be very effective in suppressing noise amplifications. Noise characteristics can be a function of the sampled frequencies or can be a single value that denotes the noise variance. Although it may not be straight-forward to obtain the signal PSD or variance \textit{a priori}, if such measurements are available / makes sense, then the implemented algorithm is able to incorporate the information for correction.

\subsection{Interface via xml}
The end-user will not have to deal with the calibration measurements $H(f)$ and $R(f)$. All the required calibration information will be provided by Varian via an xml file. The MTF information $H(f_k)$ for a specific panel will always be provided. If Varian provides a non-constant value $R(f_m)$ (function of $f_m$), then the provided $R(f_m)$ will be used by the REA algorithm. If Varian does not provide an $R(f_m)$ as a function of $f_m$, then a white noise is assumed for the input as well as the noise (\textit{i.e.}, $R(f_m) = \frac{\sigma_N^2}{\sigma_{in}^2}$). This setup allows the separation of the algorithm details and the user parameters that can be tuned: \texttt{GainAmplificationLimitDb} and \texttt{MtfAdjustmentFactor}.

\subsection{Calibration data encryption}

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{calibration_data_format}
	\caption{Calibration data format. Note that when B=0, E and F are not provided. The numbers on top indicate the number of bytes for each field.}
	\label{fig:calib_data_format}
\end{figure}
Calibration data $H(f_k)$ and $R(f_m)$ associated with each panel are formatted in a fixed format and provided in the xml file. The format used is shown in Fig.\ref{fig:calib_data_format}. Details of each field are as follows:
	\begin{itemize}
		\item A: Number of sampled points for $H(f)$, \textit{i.e.}, $K$.
		\item B: Number of sampled points for $R(f)$, \textit{i.e.}, $M$. 0 when $R(f)$ is constant.
		\item C: $f_k$ in 32-bit float format.
		\item D: $H(f_k)$ in 32-bit float format.
		\item E: $f_m$ in 32-bit format. (Optional)
		\item F: $R(f_m)$ in 32-bit format. (Optional)
	\end{itemize}

Because the calibration data are Varian proprietary information, we use a simple encryption scheme to hide the calibration data from the users. The encryption process is as follows:
\begin{enumerate}
	\item Align the calibration data into a fixed byte stream format. (A-F)
	\item Generate a 28 bytes long "salt" key (G).
	\item Append a fixed 128 bytes long key (pre-generated) to the flipped version of thei ``salt" key.
	\item Apply XOR encryption scheme to the aligned data (A-F) with the key generated in 3.
	\item The encrypted byte stream in 4 is prepended to the 28 byte ``salt" (unflipped) and encoded using the base64 encoding scheme.
\end{enumerate}
Decryption would be a repeat of the operation above in reverse order.

\section{Cubic spline interpolation}
Given $H(f_k)$ and/or $S_N(f_k), S_X(f_k)$ for all $f_k = \Delta f_{sampled}, \; k = 0, 1, \cdots, N-1$, cubic spline interpolation is used to compute the values at frequency points $f \in [-f_{Nyquist}/2, f_{Nyquist}/2]$. Assuming detector pixel size of $(\Delta u, \Delta v)$, such Nyquist frequencies can be computed, and depending on the input projection size, the newly sampled frequency points can be computed as
\begin{equation}
  f_r(i, j) = \sqrt{\left(\frac{i-N_u/2}{N_u \Delta u}\right)^2 + \left(\frac{j-N_v/2}{N_v \Delta v}\right)^2},
\end{equation}
where $(i, j) \in [0, N_u-1] \times [0, N_v-1]$, and $N_u$ and $N_v$ are the numbers of pixels in the horizontal direction and vertical directions, respectively. Note that we assume an MTF that changes only as a function of the frequency value. Therefore, a symmetric pixel result in rotationally symmetric 2D kernel, whereas, an asymmetric pixel will result in an asymmetric 2D kernel.

Cubic spline coefficients (for interpolation) can be computed by solving a system of linear equations. Specifically, the coefficients equals the least squares solution for $A \vec{m} = \vec{\nu}$, where $\vec{m} = [m_0, m_1, \cdots, m_n]^T$ holds the coefficients and $\vec{\nu} = [\nu_0, \nu_1, \cdots, \nu_n]^T$ is some function of the measurements. Different flavors of the cubic spline interpolation exist depending on the boundary conditions: natural spline, clamped spline, and not-a-knot spline. Based on the boundary condition, first and the last rows of the $(n+1)\times(n+1)$ matrix $A$ differ, but the rest of the $n-1$ rows are the same. Table \ref{tab:cubicSplineModels} illustrates the different boundary conditions and the matrix $A$ used for solving the least-squares problem.

\begin{table}[!tb]
  \centering
\begin{tabular}{lp{4.3cm}c}
  \toprule
  Spline type    & Boundary condition & Matrix $A$  \\
  \midrule
  Natural	&	Second derivatives are zero at the end points	&
  \small $
  \begin{bmatrix}
    1  &  0  &        &        &         &		      & \\
   h_0 & u_1 & h_1    &        &         &  	      & \\
       & h_1 & u_2    & h_2    &         &  	      & \\
       &     & \ddots & \ddots & \ddots  &  	      & \\
       &     &        & h_{n-3}& u_{n-2} & h_{n-2}  & \\
       &     &        &        & h_{n-2} & u_{n-1}  & h_{n-1}\\
	   &     &        &        &         &    0     &  1
  \end{bmatrix}
  $ \normalsize\\

  Clamped	&	First derivatives of the spline are specified at the end points	&
  \small $
  \begin{bmatrix}
    2h_0  & h_0 &        &        &         &		      & \\
	 h_0  & u_1 & h_1    &        &         &  	      & \\
       	  & h_1 & u_2    & h_2    &         &  	      & \\
       	  &     & \ddots & \ddots & \ddots  &  	      & \\
       	  &     &        & h_{n-3}& u_{n-2} & h_{n-2}  & \\
		  &     &        &        & h_{n-2} & u_{n-1}  & h_{n-1}\\
		  &     &        &        &         & h_{n-1}  & 2h_{n-1}
  \end{bmatrix}
  $ \normalsize\\

  Not-a-knot	&	Third derivatives of the spline are continuous at $x_1$ and $x_{N-1}$	&
  \small $
  \begin{bmatrix}
    -h_1  & u_1/2 & -h_0   &        &         &		      & \\
	      & u_1   & h_1    &        &         &  	      & \\
          & h_1   & u_2    & h_2    &         &  	      & \\
          &       & \ddots & \ddots & \ddots  &  	      & \\
          &       &        & h_{n-3}& u_{n-2} & h_{n-2}  & \\
          &       &        &        & h_{n-2} & u_{n-1}  & \\
		  &       &        &        & -h_{n-1}& u_{n-1}/2& -h_{n-2}
  \end{bmatrix}
  $ \normalsize\\

  \bottomrule
\end{tabular}
  \caption{Cubic spline models with different boundary conditions.}
  \label{tab:cubicSplineModels}
\end{table}

Matlab and Scipy uses the not-a-knot cubic spline as their default models, and therefore, the REA plugin was developed using a not-a-knot model. In the C++ REA plugin implementation, the natural spline model was used mainly due to its simplicity in solving. By design, $m_0 = m_n = 0$ for the natural spline model, and the matrix $A$ can be reduced to a $(n-1) \times (n-1)$ tri-diagonal matrix. Such tri-diagonal matrix $A$ can be easily solved via forward and back substitutions, without having to introduce more elaborate method of solving $A \vec{m} = \vec{\nu}$. The following question lingers: what is the difference/consequence of using the different spline models?

Fig.\ref{fig:threeDifferentSplineModels} shows an example of the differences between the 3 cubic spline models.
\begin{wrapfigure}{r}{0.5\textwidth}
  %\vspace{-20pt}
  \centering
  \includegraphics[width=0.4\textwidth]{threeDifferentSplineModels}
  %\vspace{-20pt}
  \caption{Example showing the differences between natural, clamped, and not-a-knot cubic spline models.}
  \label{fig:threeDifferentSplineModels}
  %\vspace{-10pt}
\end{wrapfigure}
It is interesting to note that the differences are evident only between the first and last segments of the sampled points. Fig.\ref{fig:splineInterpComparisonMtf} shows the CsI detector MTF (used for REA plugin development) at the originally sampled point (red dots), natural spline interpolated points, and not-a-knot spline interpolated points. Close-up plot of the low frequency sampled points (Fig.\ref{fig:nearDcMtf}) reveals that there are only small differences in the first couple of segments and the differences become more and more negligible. Fig.\ref{fig:splineInterpComparisonPsf} shows the detector point spread function (PSF) obtained by inverse FFT from the interpolated MTFs using 2 different cubic spline models. As Fig.\ref{fig:psfDiff} shows, the difference between the 2 PSFs is negligible when compared to the PSF magnitude (Fig.\ref{fig:psfComparison}). Therefore, \textbf{the natural cubic spline model is implemented for CST Release v1.2, without introducing any additional library to compute the least squares solution. If the need arises, the not-a-knot model can be implemented relatively easily using the Eigen matrix class} (\url{http://eigen.tuxfamily.org}).

\begin{figure}[!tb]
  \centering
  \begin{subfigure}[b]{0.47\textwidth}
	\centering
	\includegraphics[width=\textwidth]{detMTF_differentSplineBoundaryConditions}
	\caption{Overall MTF.}
	\label{fig:overallMtf}
  \end{subfigure}%
  ~ $\quad$ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
		  %(or a blank line to force the subfigure onto a new line)
  \begin{subfigure}[b]{0.47\textwidth}
	\centering
	\includegraphics[width=\textwidth]{detMTF_differentSplineBoundaryConditions_closeup}
	\caption{Closeup near DC.}
	\label{fig:nearDcMtf}
  \end{subfigure}
  \caption{Detector MTF cubic spline interpolation: natural spline vs not-a-knot spline.}\label{fig:splineInterpComparisonMtf}
\end{figure}

\begin{figure}[!tb]
  \centering
  \begin{subfigure}[b]{0.43\textwidth}
	\centering
	\includegraphics[width=\textwidth]{detPSF_differentSplineBoundaryConditions}
	\caption{PSFs.}
	\label{fig:psfComparison}
  \end{subfigure}%
  ~ $\quad$ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
		  %(or a blank line to force the subfigure onto a new line)
  \begin{subfigure}[b]{0.43\textwidth}
	\centering
	\includegraphics[width=\textwidth]{detPSF_differentSplineBoundaryConditions_closeup}
	\caption{Difference.}
	\label{fig:psfDiff}
  \end{subfigure}
  \caption{Detector PSF using cubic spline interpolated MTF: natural spline vs not-a-knot spline.}\label{fig:splineInterpComparisonPsf}
\end{figure}

\section{Boundary extrapolation prior to input projection FFT}
To utilize the fast computation of FFT for an image size that is a power of 2, it is common practice to zero-pad an image that does not have a size that is exactly power of 2. For example, a $1004 \times 1004$ image can be zero-padded to a size of $1024 \times 1024$ to be the input to an FFT computation. In CST, we use the Intel IPP library to compute the FFT of the input projection to be multiplied by the correction kernel pre-computed in the frequency domain. In IPP, there is the option of using FFT, which requires a power of 2 images size, or using DFT, which does not have such image size requirements. Having had more experience using FFT, and because of the need for a real-time processing, we decided to use the FFT routine and take a slight hit in memory to allocate a power of 2 image size.

\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.55\textwidth]{MatlabC++Comparison_edges}
  \caption{Edges preserved via boundary extrapolation instead of zero-padding. Red arrows indicate the well preserved edges.}
  \label{fig:edgePreservation}
\end{figure}

There is no guarantee that the air scan region will be zero values (more so if the input projection has yet been log-normalized, which is often the case), and that leads to problems when we blindly zero-pad the extra region to fit into a power of 2 image size.
The discontinuities at the boundaries cause edge pixel values deviations and ringing artifacts. To alleviate such artifacts from zero-padding non-zero valued boundaries, we have opted to extrapolate (linearly interpolate) the boundaries horizontally and vertically with the values linearly changing from one boundary to the other boundary. For example, the horizontal extrapolation linearly interpolates from the right edge to the left edge. Additionally, the extended image size is made to be the smallest power of 2 number that is greater that twice the image size. For example, a $2048 \times 2048$ internal image will be generated for the example $1004 \times 1004$ input projection. Fig.\ref{fig:edgePreservation} shows that the edges are well preserved (\textit{i.e.}, no drop-off in value or ringing artifacts) for both the C++ REA plugin implementation and the Matlab/Python implementation\footnote{The Matlab version by Mingshan mirrored the images to the right, bottom, and diagonal; it resulted in negligible differences near the edges.} Note that the correction has sharpened the image, and at the same time, increased the noise when compared to the input projection. More analysis will follow next.


