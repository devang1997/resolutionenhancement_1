# -*- coding: utf-8 -*-
"""
Created on Thu Sep 04 11:44:23 2014

@author: syoon
"""

import os
os.chdir("D:\Data_SYoon\Projects\ResolutionEnhancement\MTF_NPS")
import query_MTF_NPS
import matplotlib.pyplot as plt
import numpy as np
import CstImageTools as cst


db_filename = 'detector_panel_MTF_NPS.sqlite'
# Query all panels
#query = "SELECT Panel_type, ID, Scintillator, Freq_cycles_per_mm, MTF from PANEL_MTF_NPS"
# Query 1313DX panel
query = "SELECT Panel_type, ID, Scintillator, Freq_cycles_per_mm, MTF from PANEL_MTF_NPS where Panel_type=='1313X'"
query_result = query_MTF_NPS.query_sqlite(db_filename, query)
freq = [float(k) for k in query_result[0][3].split(",")]
MTF = np.array([float(k) for k in query_result[0][4].split(",")])

cst.toggle_matplotlib_params()


def plot_wiener_filter_function_G(sampled_f, H_f):

    #  0dB gain limit is equivalent to gamma = 0.25 (for R(f) = 1)
    # 10dB gain limit is equivalent to gamma = 0.0025
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(sampled_f, H_f, label=r"$H^\prime_{\alpha=1}(f)$")
    for k, regularizer_weight in zip((1, 2, 3), (0.25, 0.1, 0.05)):
        wiener_filt = H_f / (H_f**2 + regularizer_weight)
        ax.plot(sampled_f, wiener_filt, label=r"$G_{%d}$" % k)
    plt.xlabel(r"$f$ [lp/mm]")
    plt.ylabel(r"$W(f)$")
    plt.legend(loc='best')
    plt.grid()

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(sampled_f, H_f, label=r"$H^\prime_{\alpha=1}(f)$")
    for k, regularizer_weight in zip((4, 5, 6, 7), (0.025, 0.01, 0.005, 0.0025)):
        wiener_filt = H_f / (H_f**2 + regularizer_weight)
        ax.plot(sampled_f, wiener_filt, label=r"$G_{%d}$" % k)
    # Y axis ticks and label on the right side
    ax.yaxis.tick_right()
    ax.yaxis.set_label_position("right")
    plt.xlabel(r"$f$ [lp/mm]")
    plt.ylabel(r"$W(f)$")
    plt.legend(loc='best')
    plt.grid()

    plt.show()


def plot_wiener_filter_function_alpha(sampled_f, H_f, gamma=0.005):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for adj_fctr in (0.1, 0.5, 1.0, 1.5, 2.0):
        H_f_prime = H_f**(1.0 / adj_fctr)
        wiener_filt = H_f_prime / (H_f_prime**2 + gamma)
        ax.plot(sampled_f, wiener_filt, label=r"$\alpha^{-1}=%3.1f$" % adj_fctr)
    plt.xlabel(r"$f$ [lp/mm]")
    plt.ylabel(r"$W(f)$")
    plt.legend(loc='best')
    plt.grid()
    plt.show()


def compare_arithmetic_geomtric_weighting(sampled_f, H_f):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for alpha in (0, 0.5, 1.0, 1.5):
        H_f_arith = (1.0 - alpha) + alpha * H_f
        H_f_geom = H_f**(alpha)
        if alpha not in (0, 1.0):
            ax.plot(sampled_f, H_f_arith, '--', label=r"$\alpha = %3.1f$" % alpha)
        ax.plot(sampled_f, H_f_geom, label=r"$\alpha = %3.1f$" % alpha)
    plt.xlabel(r"$f$ [lp/mm]")
    plt.ylabel(r"$H^\prime_\alpha(f)$")
    plt.legend(loc='upper right')
    plt.grid()
    plt.show()
