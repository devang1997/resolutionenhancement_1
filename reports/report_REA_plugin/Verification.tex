\section{Verification}
Because all the preliminary development was done using Matlab (Mingshan) and Python (Sungwon), one of the first step after having implemented the C++ REA plugin was to verify the results with the Matlab/Python results with one set of run-time parameters ($\alpha$ and $\gamma$ in section \ref{sec:modifiedWienerDeconvModel}). After this verification with Matlab/Python, the correction kernels were inspected for different values of $\alpha$ and $\gamma$, as well as the resulting corrected projection images.

\subsection{Verification with Matlab/Python results as reference}
Verification with Matlab/Python was done on a single projection image (\texttt{det2\_dw\_int50\_hq.viv}). As Fig.\ref{fig:resolPhantomResultVerification} shows, this data is a projection of a resolution phantom commonly used in industrial NDT applications. Fig.\ref{fig:matlabC++ResolComp} compares the input projection with corrected projections processed by (1) the C++ REA plugin and (2) Matlab implementation. A line profile through the horizontally oriented resolution phantom shows that there is resolution recovery after the corrections. Comparing the C++ REA plugin result to the Matlab result, both results are indistinguishable. Although not shown, the difference image between the two results show only numerical errors, which does not result any significant image quality differences. Fig.\ref{fig:matlabC++ResolCompVert} shows a similar plot with the line profile along the vertically oriented resolution phantom. The results agree with the horizontally oriented phantom line profiles.

\begin{figure}[!tb]
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
	\centering
	\includegraphics[width=\textwidth]{MatlabC++Comparison_resolution}
	\caption{Horizontal resolution.}
	\label{fig:matlabC++ResolComp}
  \end{subfigure}
  ~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
		  %(or a blank line to force the subfigure onto a new line)
  \begin{subfigure}[b]{0.48\textwidth}
	\centering
	\includegraphics[width=\textwidth]{MatlabC++Comparison_resolutionVert}
	\caption{Vertical resolution.}
	\label{fig:matlabC++ResolCompVert}
  \end{subfigure}
  \caption{Resolution phantom after REA (C++ plugin and Matlab).}\label{fig:resolPhantomResultVerification}
\end{figure}


Fig.\ref{fig:uniformRegAnalysis} shows average horizontal line profiles of a uniform region (shown with the yellow ROI) for the input projection, C++ REA plugin corrected projection, and Matlab corrected projection. Both the corrected projections have larger fluctuation ranges when compared to the input projection; this is due to the noise amplification of the deconvolution algorithm. Again, there is no significant difference between the C++ and the Matlab implementation. Fig.\ref{fig:noiseAnalysis} shows the same set of images with the window and level narrowed to the uniform region. As the uniform region texture on the images indicate, there is an increase in high frequency components of the corrected images. The standard deviations within the defined ROI (depicted in yellow) have increased from $\sim40$ to $\sim84$ for both the C++ and Matlab implementation.

\begin{figure}[!tb]
  \centering
  \begin{subfigure}[b]{0.47\textwidth}
	\centering
	\includegraphics[width=\textwidth]{MatlabC++Comparison_uniform}
	\caption{Uniform region analysis.}
	\label{fig:uniformRegAnalysis}
  \end{subfigure}%
  ~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
		  %(or a blank line to force the subfigure onto a new line)
  \begin{subfigure}[b]{0.5\textwidth}
	\centering
	\includegraphics[width=\textwidth]{MatlabC++Comparison_noise}
	\caption{Noise analysis.}
	\label{fig:noiseAnalysis}
  \end{subfigure}
  \caption{Uniformity and noise before and after REA (C++ plugin and Matlab).}\label{fig:uniformityAndNoiseVerification}
\end{figure}



\subsection{Verification with different run-time parameters}

After verifying that the C++ REA plugin results were ``same'' as the Matlab/Python results, C++ REA plugin was investigated with different run-time parameter values. Recall that the modified Wiener deconvolution model used in the CST REA plugin (\ref{sec:modifiedWienerDeconvModel}) has two run-time parameters: $\alpha$ and $\gamma$. Parameter $\alpha$ controls the strength of the correction, and parameter $\gamma$ controls the confidence in the ratio of the noise PSD over the signal PSD. Fig.\ref{fig:correctionKernels} shows a 1D plot of the correction kernel in the spatial domain ($\textrm{FFT}^{-1}(G(f; \alpha, \gamma))$)) for different values of $\alpha$ and $\gamma$. Note that the parameter $\alpha$ has a critical impact on the correction kernel for reasonable values of the ratio of the noise PSD over the signal PSD. For the example case of $\sigma_N^2 / \sigma_X^2 = 0.01$, the correction kernel returns to a delta function (and therefore, no correction) when $\alpha = 0$. With full correction strength ($\alpha = 1$), changing the $\gamma$ from 1 to 0 results in slightly stronger correction. The extent of this stronger correction depends on the ratio of the noise PSD over the signal PSD, and therefore, the parameter $\gamma$ can control unwanted noise amplifications. Finally, by setting $\alpha = 0.5$ and $\gamma = 0.5$, the correction kernel is a trade-off between the correction kernels with extreme parameter values.

\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.45\textwidth]{correctionKernels}
  \caption{Correction kernels for different REA run-time parameters.}
  \label{fig:correctionKernels}
\end{figure}

Fig.\ref{fig:reaRtParamsResol} shows the corrected images and line profiles for 3 different values of $\alpha$ and $\gamma$ value fixed. Note that there is better recovery of resolution for larger $\alpha$ values for the case shown. Such improved resolution recovery does not come without trade-offs: noise amplification. Fig.\ref{fig:reaRtParamsRoi} depicts an ROI (yellow box) in a uniform region for noise analysis. The table in Fig.\ref{fig:reaOutputsFcnOfRtParams} shows that the standard deviation within the uniform region increases as stronger correction is applied.

\begin{figure}[!tb]
  \centering
  \begin{subfigure}[b]{0.6\textwidth}
	\centering
	\includegraphics[width=\textwidth]{ReaPluginRuntimeParams_resolution}
	\caption{Resolution comparison. (a) $\alpha = 1.0, \gamma = 1$, (b) $\alpha = 0.5, \gamma = 1$, (c) $\alpha = 0.0, \gamma = 1$}
	\label{fig:reaRtParamsResol}
  \end{subfigure}%
  ~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
		  %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.37\textwidth}
	\centering
	\includegraphics[width=\textwidth]{ReaPluginRuntimeParams_noiseRoi}
	\caption{ROI for standard deviation.}
	\label{fig:reaRtParamsRoi}
  \end{subfigure}
  \begin{tabular}{lc}
	\toprule
	Run-time parameters		&  Uniform region ROI standard deviation \\
	\midrule
	$\alpha = 1.0, \; \gamma = 1.0$	&	$78.3$ \\
	$\alpha = 0.5, \; \gamma = 1.0$	&	$50.7$ \\
	$\alpha = 0.0, \; \gamma = 1.0$	&	$40.0$ \\
	\bottomrule
  \end{tabular}
  \caption{Resolution and noise comparisons as a function of REA run-time parameters. }\label{fig:reaOutputsFcnOfRtParams}
\end{figure}

