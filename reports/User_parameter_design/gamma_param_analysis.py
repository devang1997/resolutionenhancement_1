import numpy as np
import matplotlib.pyplot as plt

# 1313DX MTF
H = np.array(
   [9.8903308e-01, 9.8312331e-01, 9.7143480e-01, 9.5545394e-01, 9.3633910e-01,
    9.1499146e-01, 8.9211057e-01, 8.6823798e-01, 8.4379146e-01, 8.1909191e-01,
    7.9438438e-01, 7.6985463e-01, 7.4564213e-01, 7.2185021e-01, 6.9855404e-01,
    6.7580690e-01, 6.5364509e-01, 6.3209176e-01, 6.1115991e-01, 5.9085480e-01,
    5.7117572e-01, 5.5211751e-01, 5.3367159e-01, 5.1582696e-01, 4.9857076e-01,
    4.8188890e-01, 4.6576641e-01, 4.5018779e-01, 4.3513725e-01, 4.2059888e-01,
    4.0655683e-01, 3.9299539e-01, 3.7989908e-01, 3.6725269e-01, 3.5504137e-01,
    3.4325062e-01, 3.3186632e-01, 3.2087475e-01, 3.1026258e-01, 3.0001689e-01,
    2.9012518e-01, 2.8057531e-01, 2.7135556e-01, 2.6245459e-01, 2.5386142e-01,
    2.4556546e-01, 2.3755645e-01, 2.2982451e-01, 2.2236005e-01, 2.1515385e-01,
    2.0819698e-01, 2.0148083e-01, 1.9499707e-01, 1.8873767e-01, 1.8269486e-01,
    1.7686117e-01, 1.7122935e-01, 1.6579242e-01, 1.6054364e-01, 1.5547650e-01,
    1.5058471e-01, 1.4586221e-01, 1.4130314e-01, 1.3690184e-01, 1.3265285e-01,
    1.2855091e-01, 1.2459093e-01, 1.2076799e-01, 1.1707734e-01, 1.1351442e-01,
    1.1007481e-01, 1.0675422e-01, 1.0354855e-01, 1.0045382e-01, 9.7466195e-02,
    9.4581957e-02, 9.1797535e-02, 8.9109474e-02, 8.6514438e-02, 8.4009209e-02,
    8.1590679e-02, 7.9255847e-02, 7.7001817e-02, 7.4825792e-02, 7.2725073e-02,
    7.0697053e-02, 6.8739218e-02, 6.6849137e-02, 6.5024467e-02, 6.3262943e-02,
    6.1562380e-02, 5.9920669e-02, 5.8335773e-02, 5.6805725e-02, 5.5328628e-02,
    5.3902649e-02, 5.2526019e-02, 5.1197030e-02, 4.9914034e-02, 4.8675438e-02,
    4.7479706e-02, 4.6325356e-02, 4.5210953e-02, 4.4135118e-02, 4.3096513e-02,
    4.2093852e-02, 4.1125890e-02, 4.0191426e-02, 3.9289302e-02, 3.8418397e-02,
    3.7577632e-02, 3.6765963e-02, 3.5982384e-02, 3.5225922e-02, 3.4495639e-02,
    3.3790629e-02, 3.3110017e-02, 3.2452959e-02, 3.1818641e-02, 3.1206274e-02,
    3.0615099e-02, 3.0044383e-02, 2.9493418e-02, 2.8961520e-02, 2.8448030e-02,
    2.7952310e-02, 2.7473746e-02, 2.7011743e-02, 2.6565729e-02, 2.6135150e-02,
    2.5719472e-02, 2.5318180e-02, 2.4930775e-02, 2.4556777e-02, 2.4195722e-02,
    2.3847162e-02, 2.3510665e-02, 2.3185813e-02, 2.2872203e-02, 2.2569446e-02,
    2.2277167e-02, 2.1995003e-02, 2.1722603e-02, 2.1459631e-02, 2.1205759e-02,
    2.0960673e-02, 2.0724069e-02, 2.0495653e-02, 2.0275141e-02, 2.0062261e-02,
    1.9856748e-02, 1.9658348e-02, 1.9466813e-02, 1.9281907e-02, 1.9103400e-02,
    1.8931071e-02, 1.8764705e-02, 1.8604097e-02, 1.8449047e-02, 1.8299362e-02,
    1.8154858e-02, 1.8015355e-02, 1.7880680e-02, 1.7750665e-02, 1.7625150e-02,
    1.7503978e-02, 1.7387000e-02, 1.7274070e-02, 1.7165049e-02, 1.7059800e-02,
    1.6958193e-02, 1.6860103e-02, 1.6765408e-02, 1.6673990e-02, 1.6585735e-02,
    1.6500534e-02, 1.6418283e-02, 1.6338877e-02, 1.6262220e-02, 1.6188215e-02,
    1.6116772e-02, 1.6047801e-02, 1.5981217e-02, 1.5916937e-02, 1.5854882e-02,
    1.5794974e-02, 1.5737140e-02, 1.5681307e-02, 1.5627406e-02, 1.5575370e-02,
    1.5525136e-02, 1.5476640e-02, 1.5429822e-02, 1.5384624e-02, 1.5340991e-02,
    1.5298867e-02, 1.5258202e-02, 1.5218943e-02, 1.5181044e-02, 1.5144455e-02,
    1.5109134e-02])

# 3024I MTF
H = np.array(
    [1.   ,  0.971,  0.944,  0.923,  0.899,  0.874,  0.848,  0.816, 0.784,  0.758,
     0.727,  0.697,  0.665,  0.636,  0.602,  0.575, 0.551,  0.522,  0.499,  0.469,
     0.449,  0.42 ,  0.396,  0.383, 0.362,  0.344,  0.319,  0.303,  0.29 ,  0.274,
     0.259,  0.249, 0.237,  0.228,  0.209,  0.201,  0.191,  0.18 ,  0.171,  0.159,
     0.154,  0.148,  0.14 ,  0.135,  0.131,  0.124,  0.118,  0.113, 0.111,  0.098,
     0.095,  0.093,  0.093,  0.088,  0.083,  0.079, 0.072,  0.069,  0.071,  0.063,
     0.065,  0.058,  0.062,  0.056, 0.051,  0.054,  0.051,  0.05 ,  0.045,  0.042,
     0.041,  0.041, 0.04 ,  0.031,  0.034,  0.03 ,  0.036,  0.029,  0.032,  0.032,
     0.022,  0.03 ,  0.026,  0.024,  0.02 ,  0.028,  0.023,  0.019, 0.014,  0.014,
     0.018,  0.015,  0.011])

R = 1e-3


# Gamma input as desired noise suppression in dB
gamma2 = []
db_list = (2, 5, 10, 15, 20, 30, 40)
for db in db_list:
    perc = 10 ** (-db / 10.0)
    H_min_avg = H[-len(H)/4:].mean()
    gamma2.append((H_min_avg**2 * (1 / perc - 1.0)) / R)

plt.figure()
for val, db in zip(gamma2, db_list):
    G = H / (H**2 + val * R)
    text = 'dB = %d' % db
    plt.plot(G , label=text)
plt.legend(loc='upper left')
plt.grid()

# Different version: target mid-high freq amplification in dB
def compute_gamma_for_target_noise_amplification(H_f, R_f, dB_list, SNR_threshold, opt=4):
    gamma_list = []
    for db in dB_list:
        perc = 10 ** (db / 10.0)
        # Option #1
        if opt == 1:
            H_min_avg = H_f[-len(H_f)/2:].mean()
            gamma = (H_min_avg * (1 / perc - H_min_avg)) / R_f
        # Option #2
        elif opt == 2:
            start_idx = -len(H_f)/2
            H_upper = H_f[start_idx:]
            tmp = H_upper * (1.0 / perc - H_upper) / R_f
            gamma = np.max((tmp.mean(), 0))
        # Option #3
        elif opt == 3:
            tmp = H_f * (1.0 / perc - H_f) / R_f
            gamma = np.max((0, tmp.max()))
        elif opt == 4:
            SNR = H_f / np.sqrt(R_f)
            idx_0 = np.nonzero(SNR < SNR_threshold)[0][0]
            tmp = H_f * (1.0 / perc - H_f) / R_f
            gamma = np.max((0, tmp[idx_0:].max()))
        gamma_list.append(gamma)

    print "Option {}:\n\tSNR threshold = {},\n\tgamma = {}".format(opt, SNR_threshold, gamma_list)
    return gamma_list


def plot_Wiener_filter(dB_list, gamma_list, H_f, R_f, SNR_threshold):
    plt.figure()
    for val, db in zip(gamma_list, dB_list):
        G = H_f / (H_f**2 + val * R_f)
        text = r'$G_{target} = $%d dB' % db
        if db >= 0:
            plt.subplot(121)
            plt.plot(G , label=text, linewidth=2)
        if db <= 0:
            plt.subplot(122)
            plt.plot(G , label=text, linewidth=2)

    for k in xrange(2):
        plt.subplot(1, 2, k+1)
        plt.legend(loc='best')
        plt.grid()
        text = r'$G(f;\, \alpha = 1.0, \, \widehat{\gamma}(G_{target})), \; R(f) =$'
        text = ''.join((text, r"%2.1e, $SNR_{threshold}$ = %d" % (R_f, SNR_threshold)))
        plt.title(text)
        plt.tick_params(axis='x', labelbottom='off')


gamma_option = 4
SNR_thresh = 5
R = 1e-1

if gamma_option in (1, 2):
    db_list = (-10, -6, -3, 0, 5, 8)
elif gamma_option in (3, 4):
    db_list = (-10, -6, -3, 0, 3, 6, 10, 12)

gamma_vals = compute_gamma_for_target_noise_amplification(H, R, db_list,
                                                          SNR_thresh,
                                                          gamma_option)
plot_Wiener_filter(db_list, gamma_vals, H, R, SNR_thresh)


# =============================================================================
# New definition of \alpha
plt.figure()
alpha_vals = (0.0, 0.3, 0.7, 1.0, 1.3)
for k in alpha_vals:
    H_prime = (1.0 - k) + k * H
    if k == 1 or k== 0:
        plt.plot(H_prime, label=r'$\alpha =$ %2.1f' % k, linewidth=2)
    else:
        text = r'Arithmetic, $\alpha =$ %2.1f' % k
        plt.plot(H_prime, '--', label=text)

for k in alpha_vals:
    H_prime =  H ** k
    if k == 1 or k== 0:
        #plt.plot(H_prime)
        pass
    else:
        text = r'Geometric, $\alpha =$ %2.1f' % k
        plt.plot(H_prime, ':.', label=text)

plt.legend(loc='best')
plt.grid()
plt.title('Modified MTF')


