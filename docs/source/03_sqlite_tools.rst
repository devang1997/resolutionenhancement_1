*******************************
Tools to Access SQLite  File
*******************************

Short List of Tools to Access SQLite File
============================================

1. `SQLite Studio <https://sqlitestudio.pl/index.rvt>`_
2. `DB Browser for SQLite <http://sqlitebrowser.org>`_

Snapshot of `detector_panel_MTF_NPS.sqlite` as of 2017 Dec 30
--------------------------------------------------------------

.. figure:: ../figs/panel_mtf_nps_table_20171230.png
    :width: 100%

    Table with the panel MTF and NPS information. (Shown only partially.)


.. figure:: ../figs/customers_table_20171230.png
    :width: 50%

    Table with the customers and linked panel information. (Shown fully.)


