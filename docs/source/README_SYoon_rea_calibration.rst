.. REA Calibration documentation master file, created by
   sphinx-quickstart on Tue Jan  2 17:00:57 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to REA Calibration's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:
   :titlesonly:
   :glob:
   :hidden:

   01_insert_data_into_DB.rst
   02_generate_encrypted_xml.rst
   03_sqlite_tools.rst


.. Indices and tables
.. ==================
.. 
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
