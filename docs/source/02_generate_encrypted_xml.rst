*************************
Encrypted XML for CST 
*************************

Encryption Algorithm
=====================

Details of the encryption algorithm used to generate the encrypted XML can
be found in the REA detailed design document. 


Example of Encryption and Decryption
======================================

Following is an adaption of a test function in `test_calibration.py` that
illustrates the usage of `create_encrypted_REA_xml` function for encrypting,
and the usage of `decode_encrypted_REA_xml` function for decrypting.

.. code-block:: python

    import os

    # Get panel information with a specified serial number
    panel = calib.get_panels_that_have(Serial_num="PKI_0822AP3")[0]
    
    # Output XML file
    output_xml_file = os.path.abspath("./rea_encrypted_file_for_testing.xml")
    
    # Encrypted xml 
    panel_enc_xml = calib.create_encrypted_REA_xml(panel, output_xml_file)

    # Check that xml file exists
    assert(os.path.isfile(output_xml_file))

    # Check the content
    with open(output_xml_file, 'rt') as file:
        read_in_txt = file.read()
        assert(read_in_txt == ''.join(panel_enc_xml))

    # Decode the encrypted
    mtf, nps = calib.decode_encrypted_REA_xml(panel_enc_xml)

    # Compare MTF and sampled freq points to reference
    assert(np.allclose(panel["Freq_cycles_per_mm"], mtf["cycles_per_mm"]))
    assert(np.allclose(panel["MTF"], mtf["spectrum"]))

    # Take care of sampled freq points for NPS
    freq_nps_key = "Freq_cycles_per_mm_NPS"
    if panel["Freq_cycles_per_mm_NPS"] is None:
        freq_nps_key = "Freq_cycles_per_mm"

    # Compare NPS and sampled freq points to reference
    assert(np.allclose(panel[freq_nps_key], nps["cycles_per_mm"]))
    assert(np.allclose(panel["NPS"], nps["spectrum"]))


Registering Customer to the Generated REA Calibration
=========================================================

After having generated an encrypted xml to be used by CST, a final step of 
linking customer and the panel for which the REA calibration has been 
generated needs to be performed for record keeping. Internally, the 
"CUSTOMERS" table within the SQLite DB file is used to link a panel to a
customer. Following is a code snippet (from `test_calibration.py`) that
illustrates the addition of a panel into the customers list DB, followed
by unlinking (removing) of the same panel from the customers list DB.


.. code-block:: python

    k_test_panel_id = 42
    panel = calib.get_panel_with_id(k_test_panel_id)

    try:
        # Add record in the CUSTOMERS table
        new_record = calib.record_calib_generation(
            panel['Panel_id'], "Ghost customer for testing purposes")

        # Separately query the CUSTOMERS table for the row with the panel id
        queried_record = calib.get_customers_with_panel_id(panel['Panel_id'])

        # Check that what's been inserted is the same as what we started with
        for key, val in new_record.items():
            assert(queried_record[key] == val)

    finally:
        # Delete the test customer record at the very end
        calib.delete_customer_with_id(new_record['xml_id'])
