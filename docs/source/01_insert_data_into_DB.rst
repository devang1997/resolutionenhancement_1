****************************
Access Data in SQLite DB
****************************

Retrieve MTF Information from Online and Insert into the DB
=============================================================

This section details a step-by-step process to retrieve MTF data from the web,
update the SQLite database with the retrieved MTF data, and finally generate
a plot of the retrieved MTF curves.


Locate final test MTF data for a given panel with a serial number
--------------------------------------------------------------------

The process for retrieving serial numbers for a given panel type and customer is currently a manual process.

Online tool to retrieve panel information based on panel type and customer name can be found:

	1. `Recent measurements <http://slfpweb2.vms.ad.varian.com:8093/Receptor_Test_Reports>`_ 
	2. `Archived measurements <http://slfpfs/fptestdata-archive/Imagers>`_ (more on this later)

.. _select_panel_and_customer:
.. figure:: ../figs/ReceptorTestReport_Select01.png
	:width: 60%

	Select panel type and customer.
  

.. _select_MTF:
.. figure:: ../figs/ReceptorTestReport_Select02.png
	:width: 70%

	Select MTF info.


.. _collect_serial_numbers:
.. figure:: ../figs/ReceptorTestReport_Select04.png
	:width: 80%

	Collect serial numbers for interested panels.

 
Example run-through for finding information for the Curvebeam 3030D panel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Go to the `starting URL <http://slfpweb2.vms.ad.varian.com:8093/Receptor_Test_Reports>`_ 
2. Select the panel type and customer information (there may be multiple Rev versions) and click on `VIEW INDIVIDUAL TESTS` (see :numref:`select_panel_and_customer`)
3. Select the MTF information item and click on `VIEW SELECTED TESTS` (see :numref:`select_MTF`)
4. **Write down serial number(s) of panel(s) that you are interested in** (see :numref:`collect_serial_numbers`)

 	* if multiple, write them all down
   	* serial numbers will be used to retrieve the MTF information using the python rea package


Archived measurements
^^^^^^^^^^^^^^^^^^^^^^

In :numref:`collect_serial_numbers`, you can click on the panel serial number to inspect the MTF data. Due to an archiving scheme used to store the information, if the measurement date is "old," then the data is move to a different URL. For example, two different panels are highlighted in :numref:`collect_serial_numbers`.
One of the panels has a test date of 1/18/2015, and the other one has a test date of 12/15/2017. At the time of this writing, the former panel is categorized as an "old" test date, but the latter is not. Therefore, when the former panel serial number is clicked, a "Not Found" page will appear, where the URL is as follows: 

    http://slfpweb2.vms.ad.varian.com:8093/Imagers/E---/E1--/E158-10

Archived test data of the panel actually lives in the following URL (where the URL shows the location of the actual MTF measurement information): 

    http://slfpfs/fptestdata-archive/Imagers/E---/E1--/E158-10/final/pass_1/Receptor%20Test%20Data/MTF

Recent measurements
^^^^^^^^^^^^^^^^^^^^^^

The latter panel MTF information still lives on the non-archive URL, thus the latter panel MTF information can be found (at the time of this writing) at the following URL by simply clicking on the serial number: 

    http://slfpweb2.vms.ad.varian.com:8093/Imagers/4---/42--/426S07-1804/final/pass_1/Receptor%20Test%20Data/MTF

.. note::

    When the python rea package is used for retrieving the MTF from the Varex factory floor measurements, all of the archived and non-archived url is automatically taken care of. Therefore, it is easier to use the python rea package to retrieve the MTFs.


Retrieve MTF for the located serial numbered panel (from the web) and update the SQLite DB
---------------------------------------------------------------------------------------------

Having retrieved the desired panel serial numbers, python rea package can now be used to 

1. retrieve the MTF and associated information from the test results web pages
2. parse the MTF and relevant information as needed
3. insert the MTF and associated information into the SQLite DB

Following is an example code that achieves the aforementioned steps.

.. code-block:: python

    import rea

    # Panel type and customer (Yxlon)
    k_test_report_id = "3030DX-I YXN Rev AC 82955"

    # Serial numbers from online tool to locate the panels
    k_manually_identified_serial_nums = (
        "626S06-2002", "601S04-2002"
        )

    # Create a REA calibration instance
    # Implicitly assumes detector_panel_MTF_NPS.sqlite as the DB file
    calib = rea.calibration()

    # Fetch MTF information based on the serial numbers and update the database
    calib.add_new_panel_from_online(k_test_report_id,
                                    k_manually_identified_serial_nums,
                                    "3030DX-I", "Scintillator Test Only")


Plot retrieved MTFs
----------------------

Another analysis tool available in the rea package is the ability to plot MTF and NPS of multiple panels for comparison. Following is a simple code snippet that can be used to compare the MTFs of 2 panels with given serial numbers. One can also plot all the MTFs of panels associated with a customer. Such examples are available in the calibration test code (`test_calibration.py`).

.. code-block:: python

    # Collect panel information
    panel_info = list()
    for curr_serial_num in k_manually_identified_serial_nums:
        new_panel_info = calib.get_panels_that_have(serial_num=curr_serial_number)
	    if new_panel_info:
            panel_info.append(new_panel_info)

    # Plot MTFs
    calib.plot_MTF_NPS(panel_info, False)


Remove panel information with serial number from the SQLite DB
-----------------------------------------------------------------
Removing a "row" in the SQLite DB that is associated with a panel with a 
specific serial number is also possible using the rea package, as shown in
the following code snippet.

.. code-block:: python

    # Remove panel information with specified serial numbers
    for curr_serial_num in k_manually_identified_serial_nums:
        calib.delete_panel_with_serial_num(curr_serial_num)


.. raw:: latex

    \newpage


Insert MTF and/or NPS Information from Local Source
====================================================

This section details a step-by-step process for inserting MTF / NPS data from
a local source, such as an excel file with MTF / NPS measurements. 

Following code snippet is an adaptation of a test function in `test_calibration.py` 
that illustrates how a locally sourced MTF / NPS information along with other panel 
related information is added to the SQLite DB. The example shows randomly
generated MTF and NPS values, but in reality, these values will usually come from
excel or csv file. Details of how to process a csv and excel files within Python3
will not be shown here; multiple examples and methods can be found online.

.. code-block:: python

    # Bogus panel information
    test_panel = {
        "Panel_type": "test_panel_nonexisting",
        "Serial_num": "12345^&*()",
        "Scintillator": "magic_material",
        "Pixel_size_mm": 0.111,
        "Freq_cycles_per_mm": np.random.rand(100),
        "MTF": np.random.rand(100),
        "Freq_cycles_per_mm_NPS": np.random.rand(71),
        "NPS": np.random.rand(71),
        "Notes": "Bogus panel info for testing purposes only."
    }
    
    try:
        # Insert a new item
        calib.add_new_panel(test_panel["Serial_num"],
                            test_panel["Panel_type"],
                            test_panel["Scintillator"],
                            test_panel["Pixel_size_mm"],
                            test_panel["Freq_cycles_per_mm"],
                            test_panel["MTF"],
                            freq_nps=test_panel["Freq_cycles_per_mm_NPS"],
                            nps=test_panel["NPS"],
                            notes=test_panel["Notes"])

        # Retrieve the inserted panel from DB
        inserted_panel = calib.get_panel_with_serial_num(test_panel["Serial_num"])

        # Compare values against reference
        for key, val in inserted_panel.items():
            if val and key not in "Panel_id":
                # Check per element for lists
                if isinstance(val, list):
                    assert(np.allclose(test_panel[key], val))
                else:
                    assert(test_panel[key] == val)
    finally:
        # Delete the inserted item
        calib.delete_panel_with_serial_num(test_panel["Serial_num"])
