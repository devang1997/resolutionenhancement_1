REA Calibration 
===================

Version 1.0
---------------------------------
* 2018 - 

* rea package 

    * calibration.py
        * ResolEnhAlgCalib class
        * Member functions to interface with the sqlite3 DB file: add/remove panel information and customere information
        * Member functions to create encrypted xml files to be consumed by CST
        * Member (static) functions to plot MTF and NPS in the sqlite3 DB
        * Member function to retrieve MTF measurements (from factory floor) stored online

    * encrypt_calibration_data.py: encryption of the MTF and NPS using base64 and/or xor and/or salt key
    * retrieve_MTF_factory_data.py: MTF retrieval from online DB (mainly by Alan Brooks)

* Test functions (using pytest) to test calibration and encryption


Prev version 1.0
----------------------------------
* 2016 - 2017

* Python codes for file/data retrieval written by Alan (before he left)

    * retrieve_MTF_factory_data.py
    * example_download_factory_data.py

* New codes / structure

    * Only retrieve_MTF_factory_data.py is used from Alan's code (for now)
    * Moved code from query_MTF_NPS.py to ReaCalib.py
    * Created Test folder

        * test_ReaCalib.py: currently has limited testing for ReaCalib
        * test_retrieve_MTF_factory_data: test functions originally in retrieve_MTF_factory_data.py moved inside this file

