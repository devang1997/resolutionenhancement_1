# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages

# Delete previous builds!
# >rm -rf build/ dist/ src/rea.egg-info/

# >python setup.py bdist_wheel
# 
# Binary distribution.
#  Does not include documentation (pdf) nor the sqlite3 DB file.


# >python setup.py sdist
# 
# Source distribution.
#  Includes documentation, sqlite3 DB file, and test functions & data
#  via MANIFEST.in file.

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(

    name='rea',
    description='Resolution enhancement algorithm (REA) calibration and sqlite3 DB maintenance tool',
    long_description=readme,

    version='1.0.0',
    author='Sungwon Yoon, Alan Brooks',
    author_email='sungwon.yoon@vareximaging.com',
    #url='https://',
    license=license,

    #package_dir={'': '.'},
    package_dir={'': 'src'},
    packages=find_packages(#where='.', 
                           #exclude=('docs', 'reacalibration_PREV')
                           where='src',
                           exclude=['reacalibration_PREV'],
                           ),
    # From https://setuptools.readthedocs.io/en/latest/setuptools.html#including-data-files
    # Used to include the xml skeleton file under rea/data folder
    package_data={
        # If any package contains *.xml files, include them:
        #'': ['*.xml'],
        # And include any *.xml files found in the 'data' subdirectory
        # of the 'mypkg' package, also:
        'rea': ['data/*.xml'],
    },
    # Does NOT work!
    #include_package_data=True,
    

    install_requires=[
          'bs4',
          'requests',
          'json',
          'collections',
          'urlib',
          'sqlite3',
          'datetime',
          're'
      ],
      tests_require=['pytest', 'pytest-mpl'],
)
